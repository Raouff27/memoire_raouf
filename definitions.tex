\chapter{Définitions et notations}\label{C:definitions}

Dans ce chapitre, nous introduisons les définitions et les notions générales relatives au problème TCCM utilisées au cours de ce mémoire. Nous commençons par la définition des différents concepts qui ont trait aux algorithmes et leur complexité, en parlant de l'efficacité des algorithmes et la relation entre les différentes classes de complexité. Nous discutons aussi de la réduction polynomiale des problèmes NP-complets et de la grande problématique de la théorie de la complexité. Ensuite, nous rappelons la définition d'algèbre de Boole, des fonctions booléennes et du problème de satisfaisabilité. Et, nous terminons par des notions classiques sur les graphes. Les principales ressources sur lesquelles nous nous basons dans la description de ce chapitre sont \cite{Liedloff2007, Mary2013, Delisle2015, Tourniaire2013, Bachelet2011, Lopez2008}.

\section{Algorithmes, complexité et NP-complétude}

Un \emph{algorithme} est une suite finie et non ambiguë d’opérations ou d'instructions permettant de résoudre un problème donné. Il peut être simple ou compliqué dépendamment de la difficulté du problème à traiter mais il doit obtenir une solution en un nombre fini d'étapes. Un algorithme doit être correct, bien structuré et le temps nécessaire à son exécution ne doit pas être trop important. Il doit être simple de le traduire en un langage de programmation pour produire un programme exécutable. Parmi les problèmes que l’on est susceptible de résoudre par un algorithme, on peut distinguer plusieurs types :
\begin{enumerate}
 	\item Les problèmes de décision : déterminer si le problème possède une solution ou non.
 	\item Les problèmes de recherche : trouver une solution et la retourner si elle existe.
 	\item Les problèmes de comptage : calculer le nombre de solutions d’un problème.
 	\item Les problèmes d’énumération : calculer toutes les solutions d'un problème.
 \end{enumerate}
Dans plusieurs cas, ces problèmes sont tellement complexes qu’il n’est pas possible à l’être humain de compter uniquement sur la puissance de son ordinateur pour les résoudre. Ainsi, pour un problème donné, on cherche toujours à avoir l'algorithme le plus efficace possible. Cette efficacité dépend de ce qu'on cherche à optimiser: il peut s'agir du temps d’exécution de l’algorithme dans le pire cas, le temps d’exécution moyen de l’algorithme, ou encore l’espace mémoire qu’il utilise.

Généralement, l'\emph{efficacité} d'un algorithme dépend crucialement de la difficulté du problème à résoudre car le degré de difficulté varie d'un problème à l'autre. Afin de pouvoir classer les problèmes selon leur difficulté et de mesurer l'efficacité d'un algorithme résolvant un problème donné indépendamment de la machine et du matériel utilisé, une notion dite \emph{théorie de la complexité} a été introduite. Cette théorie, qui s'est beaucoup développée au cours des cinquante dernières années, permet d'évaluer le nombre d'opérations nécessaires à un algorithme pour s’exécuter et ainsi de classifier les problèmes informatiques selon leur difficulté.

La complexité d’un algorithme est généralement mesurée par une fonction $T(n)$, qui indique le nombre d'opérations effectuées lors de l’exécution de l’algorithme en fonction de la taille $n$ des données en entrées (c’est-à-dire les paramètres que l’on donne à l’algorithme). Quand la taille des données du problème traité devient de plus en plus grande, on calcule la complexité asymptotique (à l'aide de la notation \emph{grand-$O$}) plutôt que d'utiliser une mesure exacte du temps d’exécution, ce qui est d'ailleurs impossible puisque le temps réel d'exécution dépend de la machine utilisée, du langage de programmation, etc.

Le critère du temps n’est pas toujours celui qui nous intéresse. On peut aussi vouloir estimer l’espace mémoire utilisé par un algorithme. Dans ce cas, on parle de \emph{complexité spatiale}. Le tableau \ref{comp} représente les différentes classes de la complexité selon le grandeur $O$ (de la plus complexe à la plus simple). 
\begin{table}[!ht]
\setlength{\arrayrulewidth}{0.5mm}
\setlength{\arrayrulewidth}{0.3mm}
\doublerulesepcolor{black}
\begin{center}
\begin{tabular}{ |l|l|m{6cm}|l|}
\hline \rowcolor{black!30}
\textbf{Complexité} & \textbf{Vitesse} & \textbf{Temps} & \textbf{Grandeur $O$} \\
\hline \rowcolor{blue!10}
Factorielle & très lent & proportionnel à $n^n$. & $O(n!)$\\ 
\hline \rowcolor{blue!10}
Exponentielle & lent & proportionnel à une constante $k \geq 2$ à la puissance $n$. & $O(k^n)$ \\ 
\hline \hline \rowcolor{green!10}
Polynomiale & moyen & proportionnel à $n$ à une puissance donnée $k \geq 2$. & $O(n^k)$ \\
\hline \rowcolor{green!10}
Quasi-linéaire  & assez rapide & intermédiaire entre linéaire et polynomial. & $O(n log(n))$ \\
\hline \rowcolor{green!10}
Linéaire & rapide & proportionnel à $n$. & $O(n)$ \\
\hline \rowcolor{green!10}
Logarithmique & très rapide & proportionnel au logarithme de $n$. & $O(log(n))$ \\
\hline \rowcolor{green!10}
Constante & le plus rapide & indépendant de la donnée. & $O(1)$ \\
\hline
\end{tabular}
\end{center}
\caption [Les classes de la complexité temporelle selon le grandeur $O$]{Les classes de la complexité temporelle selon la grandeur $\textbf{O}$.}
\label{comp}
\end{table}

On dit qu'un algorithme est \emph{polynomial} (ou, dans notre cas, \emph{efficace}) si sa complexité est bornée dans le pire cas par un polynôme ayant la taille des données d'entrées comme variable. Ainsi, l'algorithme est efficace s'il reste d'ordre polynomial même si la taille des données en entrée devient très importante. Dans le tableau \ref{comp}, les algorithmes de type  \emph{factoriel} et \emph{exponentiel} ne sont pas efficaces car le temps d’exécution explose lorsque la taille des données d'entrée augmente. En revanche, les autres classes sont utilisables.
\begin{example}
Considérons les algorithmes $A$, $B$ et $C$ ayant les complexités montrées dans le tableau \ref{T:complexité}.
\begin{table}[tbph]
\setlength{\arrayrulewidth}{0.4mm}
\renewcommand{\arraystretch}{1}
\begin{center}
\begin{tabular}{|m{3cm}|m{2cm}|m{2cm}|m{2cm}|}
\hline
Algorithme & A & B & C \\
\hline
Complexité & 80 $\cdot$ n & 10 $\cdot$ $n^2$ & n! \\
\hline
Pour n=4 & 320 & 160 & 24 \\
\hline
Pour n=20 & 1600 & 4000 & $24 \cdot 10^{18}$ \\
\hline
\end{tabular}
\end{center}
\caption[Tableau comparatif entre la complexité des algorithmes A, B et C.]{Tableau comparatif entre la complexité des algorithmes A, B et C pour n=4 et n=20.}
\label{T:complexité}
\end{table}

\vspace*{-0.3cm}
L'algorithme le plus efficace pour 4 éléments est l'algorithme $C$. Mais, pour 20 éléments, on s’aperçoit tout de suite que l’algorithme $C$ n’est plus utilisable. Par contre, $A$ et $B$ restent applicables. Cet exemple justifie le fait que, si le nombre d’opérations n’explose pas avec une augmentation de la taille des données en entrée, l’algorithme est considéré efficace.
\end{example}
La théorie de la complexité se concentre sur l'étude de la complexité des différents types de problèmes (décision, optimisation, dénombrement,...). Pour des raisons de simplicité, on limite notre étude sur la complexité des problèmes de décision. \emph{Un problème de décision} (ou décisionnel) est un problème dont la réponse est oui ou non. On représente fréquemment un problème de décision par un ensemble $X$ qui est un sous-ensemble de $Y$. Alors, pour chaque instance $y \in Y$, le problème consiste à décider si $y \in X$ ou non. Cela justifie que les problèmes qui ne sont pas décisionnels, comme les problèmes d’optimisation, peuvent être transformés en un problème de décision de difficulté équivalente ou supérieure.
\begin{example}
Le problème de décision TCCM est formulé comme suit :\\
\emph{\textbf{Instance:}} Un graphe orienté $G = (V,A)$, un entier $k$;\\
\emph{\textbf{Question:}} Le graphe $G$ possède t-il un transversal $S$ de $k$ sommets $ S= \{v_1,v_2,...,v_k\}$, c'est-à-dire que la suppression des sommets de $S$ casse tous les circuits de $G$ ?
\end{example}
La théorie de la complexité permet de diviser les problèmes décisionnels selon le temps d'exécution nécessaire à leur résolution en plusieurs classes, les plus connues étant les suivantes.
\begin{enumerate}
	\item \textbf{La classe P.} C'est la classe des problèmes décidables par une machine de Turing déterministe en temps polynomial par rapport à la taille des donnée en entrée (le plus court chemin, l'arbre de poids minimal, flot maximal, etc.).\label{PNP}
	\item \textbf{La classe NP.} Il s'agit de la classe des problèmes décidables par une machine de Turing non déterministe en temps polynomial par rapport à la taille de l’entrée (systèmes d’équations linéaires, problèmes des nombres premiers, cycle hamiltonien, etc.).
	\item \textbf{La classe NP-complets.} Un problème de décision est NP-complet lorsqu'il est NP, et s'il est au moins aussi difficile que tous les autres problèmes NP (satisfaisabilité d’une formule booléenne, problème de sac à dos, voyageurs de commerce, problème du coloriage de graphe, etc.).
	\item \textbf{La classe NP-difficiles :} est la classe des problèmes qui sont au moins aussi difficiles que tout problème de NP (problème de l'arrêt, somme des sous-ensembles, transversaux de circuits de cardinalité minimale, etc.).
	\end{enumerate}

Les problèmes NP-complets et NP-difficiles sont souvent confondus, la différence entre ces classes de problèmes est qu'un problème NP-difficile peut ne pas être dans NP et n'est pas nécessairement un problème de décision. Les problèmes NP sont généralement difficiles à résoudre, le souci reste celui de trouver une technique de résolution dont le temps augmente polynomialement lorsque la taille du problème à résoudre augmente. En fait, la problématique centrale la plus connue, en théorie de la complexité, est la fameuse question : Est ce que P = NP ? En d’autres termes, est-ce qu’un problème NP-complet peut être résolu en temps polynomial ? Ce problème est ouvert depuis que Stephen Cook a démontré son théorème en 1971 qui dit que le problème de satisfaisabilité d'une formule booléenne (SAT) est NP-complet et que tous les problèmes de la classe NP sont polynomialement réductibles au problème SAT \cite{Cook1971}. En 2001, le problème P = NP ? a été classé parmi les sept problèmes non résolus jugés les plus importants et dotés chacun d'un prix d'un million de dollars.

Le théorème de Cook a permis d'identifier beaucoup d'autres problèmes comme étant NP-complets par l'application des réductions polynomiales. Ainsi, pour prouver qu'un problème est NP, il suffit de démontrer qu'il est polynomialement réductible à un problème NP déjà connu, donc l’équivalence entre les problèmes NP est généralement obtenue à l’aide de réductions polynomiales.

Plus précisément, on dit qu'un problème ${\Pi}_1$ est \emph{polynomialement réductible} à ${\Pi}_2$ (noté  ${\Pi}_1 <_p {\Pi}_2$) s’il existe une fonction $f$ calculable en temps polynomial telle que toute instance $I$ de ${\Pi}_1$ admet une solution si et seulement si $f(I)$ admet une solution pour le problème ${\Pi}_2$.

Un problème de décision ${\Pi}_1$ est dit \emph{NP-complet} si, ${\Pi}_1 \in NP$  et $X<_p {\Pi}_1 , \forall X \in NP $. Si ${\Pi}_1$ est polynomialement réductible à ${\Pi}_2$ et si ${\Pi}_2$ est résolu en un temps polynomial, alors ${\Pi}_1$ est également résoluble en temps polynomial. \label{red_pol}

La figure \ref{npnp} représente les frontières entre les différentes classes de la complexité. La partie gauche représente le cas où les classes P, NP et NP-complet sont différentes et la partie droite représente le cas où les classes P, NP, NP-complet sont égales. 
\begin{figure}[!ht]
\centering
\includegraphics [scale=0.7]{Logos/NP}
\caption [Diagrammes de Venn des différentes classes de problèmes.]{Diagrammes de Venn pour les classes des problèmes P, NP, NP-complets et NP-difficiles dans le cas où $P=NP$ et le cas $P \neq NP$. \url{https://en.wikipedia.org/wiki/NP-hardness}.}
\label{npnp}
\end{figure}

Jusqu'à ce jour, on ne peut donc pas affirmer s’il existe ou non un algorithme polynomial pour résoudre chacun des problèmes NP. On sait que P est inclus dans NP mais rien ne nous permet d'affirmer que P n'est pas NP. La réponse à cette question permettrait de résoudre plusieurs autres grandes questions de la théorie de la complexité. En effet, l’obtention d’un algorithme polynomial pour résoudre \emph{un seul} problème NP-complet permettrait immédiatement de résoudre \emph{n'importe quel} problème NP en un temps polynomial. Inversement, si l’un d’eux est prouvé être intraitable en temps polynomial, alors la classe NP est distincte de la classe P. L’attente de la plupart des informaticiens et mathématiciens est que l’égalité soit fausse. Malheureusement, il n’existe pas d'argument rigoureux appuyant cette intuition.
 
\section{Algèbre de Boole et fonctions booléennes}

L'algèbre de Boole forme une composante importante de l'informatique et de la conception des systèmes numériques. Soit un ensemble des valeurs de vérité \B~$= \{0, 1\}$ où $0$ représente la valeur faux et $1$ la valeur vraie. L'ensemble \B\ muni des opérateurs logiques \emph{et} ($\wedge$), \emph{ou} ($\vee$) et \emph{non} ($\neg$) forme une \emph{algèbre de Boole}. Soit $n$ un entier supérieur ou égal à 1. On note \F$_n$ l’ensemble des fonctions $f :$ \B$^n \longrightarrow$ \B\ et \F $ =  \bigcup \limits _{n=0}^{\infty} \mathds{F}_n$. L'ensemble \F\ muni des mêmes opérateurs que \B, est aussi une algèbre de Boole. 

Une fonction définie sur une algèbre de Boole est appelée \emph{fonction booléenne}. Une \emph{fonction booléenne} (ou \emph{fonction binaire}) à $n$ variables $x_1$, $x_2$, $\ldots$, $x_n$ est donc toute application $f$ définie de \B$^n$ vers \B\ comme suit~:
$$ \emph{f} : \mathbb{B}^n \longrightarrow \mathbb{B}$$
$$(x_1, x_2, ..., x_n) \longmapsto \emph {f} (x_1, x_2, ..., x_n).$$
Une telle fonction booléenne $f$ est entièrement définie par la donnée d'une table associant à chacune des $2^n$ valeurs possibles pour le n-uplet ($x_1$, $x_2$, $\ldots$, $x_n$) la valeur de $ f(x_1$, $x_2$, $\ldots$, $x_n)$.

Pour la simplification des fonctions booléennes et pour mieux comprendre le fonctionnement de certains circuits usuels on utilise souvent la \emph{décomposition de Shannon}. C’est en effet sur ce théorème de décomposition que s’appuient la plupart des techniques modernes de manipulation et d’optimisation des fonctions booléennes. Le théorème de Shannon a rendu le traitement d'une fonction booléenne d'une manière récursive assez facile ainsi que sa décomposition par rapport à ses variables. 

Si une variable $x_i$ de la fonction $f$ est remplacée par une constante $b$, la fonction résultante est appelé une \emph{restriction} ou \emph{cofacteur} de $f$ par rapport à $x_i$, et noté $f|_{x_i=b}$. Pour toute variable $x_i \in \{x_1$,$x_2$,...,$x_i$,...,$x_n\}$ on a :
$$f|_{x_i=b}(x_1,...,x_i,...,x_n)=f(x_1,...,x_{i-1},b,x_{i+1}...,x_n).$$
Ainsi, pour toute variable $x_i$ de $X=\{x_1,x_2,...,x_i,...,x_n\}$, la \emph{décomposition de Shannon} de $f$ sur la variable $x_i$ s'énonce par la formule
$$f (x_1,x_2,...,x_i,...,x_n) = (\neg{x_i}\wedge f (x_1,x_2,...,0,...,x_n)) \vee (x_i \wedge f(x_1,x_2,...,1,...,x_n))$$
\label{shan} 
Si on introduit la notion du cofacteur dans la décomposition de Shannon de $f$ sur $x_i$, on obtient 
$$f (x_1,x_2,...,x_i,...,x_n) = (x_i \wedge f|_{x_i=1}) \vee (\neg{x_i} \wedge f|_{x_i=0}) $$ 
où $f|_{x_i}$ est le cofacteur de $f$ par rapport à $x_i$.

Plusieurs problèmes de conception logique, de vérification, d'intelligence artificielle et de combinatoire peuvent être exprimées comme une séquence d'opérations par des fonctions booléennes. Cependant, plusieurs problèmes liés à ces fonctions tels que le problème de satisfaisabilité ($SAT$), le problème de vérification de la tautologie ou l'équivalence de deux expressions booléennes sont NP-complets ou coNP-complets (complément d'un problème NP-complet). Parmi ces problèmes les plus importants, le problème SAT qui consiste à décider si une formule booléenne mise sous forme normale conjonctive (voir définition \ref{fnc}) admet ou non une valuation qui la rend vraie.

\label{sat}
Le problème $SAT$ est formulé comme suit : Étant donné un ensemble $X=\{x_1,x_2,...,x_n\}$ de $n$ variables, chaque variable peut prendre deux valeurs (vrai, faux). Un \emph{littéral} est une expression logique formée d’une seule variable $x_i$ ou de sa négation $\neg x_i$ avec $i \in [1..n]$. Une \emph{clause} est une \emph{expression logique} formée uniquement de la disjonction de littéraux (exemple : $x_1 \vee \neg x_2 \vee x_3$). Soit $m$ clauses $C_i$ formées à l'aide de $n$ variables (littéraux) et la \emph{formule} $\Phi = C_1 \wedge C_2 \wedge ... \wedge C_m$. Le problème $SAT$ consiste à savoir si on peut affecter une valeur à chaque variable $x_i$ telle que la valeur de la formule $\Phi$ soit vraie.

Le problème $SAT$ a plusieurs variantes. On citera $k$-SAT dans lequel on cherche la satifaisabilité d’une formule constituée d’une conjonction de disjonctions de $k$ littéraux. Le problème $3$-SAT (pour $k=3$) est particulièrement utile et il est démontré lui aussi NP–complet. Pour le démontrer, il suffit de prouver que le problème $SAT$ est polynomialement réductible à $3$-SAT. En revanche, $2$-SAT est de classe P.

Beaucoup de problèmes d’optimisation dans différents domaines comme la vérification formelle de circuits, la cryptographie, la configuration de produits, la planification, et même la bio-informatique, peuvent se réduire assez naturellement à l’une ou l’autre des variantes de $SAT$ et ainsi peuvent être modélisés par des formules booléennes. La complexité pour résoudre ces problèmes a fait l’objet de nombreuses publications. À l’heure actuelle, on ne connaît pas d’algorithme meilleur que $O(2^n)$ en pire cas pour résoudre $SAT$ \cite{Tourniaire2013}.

\section{Graphes}

Les graphes sont des structures de données qui jouent un rôle fondamental en informatique et en mathématiques. Ils permettent de représenter naturellement de nombreux problèmes et ils sont considérés comme l’un des instruments les plus efficaces pour résoudre des problèmes posés au sein de différentes disciplines telles que la chimie, la biologie, les sciences sociales et les réseaux. Ils permettent de représenter simplement la structure, les connexions, les relations, les dépendances et les cheminements possibles entre les éléments d’un ensemble comprenant un grand nombre de situations. 

Un \emph{graphe} est un ensemble fini de sommets (nœuds dans le contexte des réseaux) et d'arcs (arêtes dans le cas des graphes non orientés) défini comme un couple $G=(V,A)$ où $V =\{v_1,v_2,...,v_n\}$ est l’ensemble des sommets et $A=\{a_1,a_2,...,a_m\}$ l’ensemble des arcs. Un \emph{arc} $a_1=(v_1,v_2)$ est un élément du produit cartésien $V \times V = \{(v_1,v_2) \mid v_1,v_2 \in V\}$. Le sommet $v_1$ est l’\emph{extrémité initiale} de l'arc et le sommet $v_2$ l’\emph{extrémité finale} (ou bien \emph{origine} et \emph{destination}).
\begin{figure}[!ht]
\centering
\subfigure[\label{gga}]{\includegraphics [scale=1]{Logos/graphe_oriente}}
\hspace*{3cm}
\subfigure[]{\includegraphics [scale=1]{Logos/graphe_non_oriente}}
\caption[Graphe orienté et le graphe non orienté lui associé.]{Graphe orienté et le graphe non orienté lui associé. (a) Graphe orienté $G=(V,A)$, l'arc $u=(v_3,v_3)$ est une boucle. (b) Le graphe non orienté induit du graphe orienté de la figure \ref{gga}.}
\label{gg}
\end{figure}

Dans beaucoup d’applications, les relations entre les éléments d’un ensemble sont orientées, un élément $x$ peut être en relation avec un autre $y$ sans que $y$ soit nécessairement en relation avec $x$, on parle alors de \emph{graphes orientés}. Dans d'autres cas, l’orientation des arcs ne joue aucun rôle, on s’intéresse simplement à l’existence d’arc entre deux sommets (sans en préciser l’ordre), dans ce cas, on parle de \emph{graphes non orientés}. Dans ce mémoire, on s'intéresse à l'étude du problème TCCM dans les graphes orientés.

Soit $G = (V,A)$ un graphe orienté, $V$ est un ensemble fini de sommets, et $A \subseteq (V \times V)$ l'ensemble des arcs. On note $n = |V|$ le nombre de sommets et $m = |A|$ le nombre d'arcs. Pour tout sous-ensemble $U \subseteq V$, on note $G[U]$ le \emph{sous-graphe induit} par $U$ sur $G$, et $A(U) = A \cap U^2$ les arcs de $G[U]$. Soit $u \in V$ un sommet, on note $N^-(u)= \{v \in V \mid (v,u) \in A\}$ l'ensemble des \emph{prédécesseurs} de $u$, et $N^+(u)= \{v \in V \mid (u,v) \in A\}$ l'ensemble des \emph{successeurs} de $u$. Également, on note  $A^-(u)= \{(v,u) \mid v \in N^-(u) \}$ l'ensemble des arcs entrants à $u$, et $A^+(u)= \{(u,v)\mid v \in N^+(u) \}$ l'ensemble des arcs sortants de $u$. Deux sommets sont \emph{adjacents (ou voisins)} s’ils sont joints par un arc. Deux arcs sont \emph{adjacents} s’ils ont au moins une extrémité commune. L'ensemble de sommets \emph{adjacents (voisins)} de $u$ est $N(u)= \{N^-(u) \cup N^+(u)\}$, et l'ensemble d'\emph{arcs adjacents} de $u$ est $A(u)= \{A^-(u) \cup A^+(u)\}$. \emph{Le degré intérieur} $deg^-(u) = |N^-(u)|$ de $u$ est le nombre d’arcs ayant $u$ comme extrémité finale. \emph{Le degré extérieur} $deg^+(u) = |N^+(u)|$ de $u$ est le nombre d’arcs ayant $u$ comme extrémité initiale. \emph{Le degré} d'un sommet $u$ est $deg(u)= deg^-(u) + deg^+(u)$. Un sommet $u$ est une \emph{source} si $deg^-(u) = 0$. Un sommet $u$ est un \emph{puits} si $deg^+(u) = 0$. 

Dans un graphe orienté $G=(V,A)$, un \emph{chemin} est une suite de sommets $(u_0,u_1,...,u_k)$ tel que $(u_i,u_{i+1}) \in A$ pour $i=0,1,...,k-1$. Le nombre $k$ est appelé la \emph{longueur du chemin}. Un \emph{circuit} $c$ est un chemin non vide qui commence et termine par le même sommet. Un graphe orienté est \emph{acyclique} s'il ne contient aucun circuit. \emph{Un transversal de circuits} est un sous-ensemble de sommets $U \subseteq V$ ayant la propriété que le sous-graphe induit $G'= (V-U, A \cap (V-U)^2)$ est acyclique. Un transversal de circuits est dit \emph{minimum} s'il est de taille minimale. 

Un graphe $G=(V,A)$ est \emph{connexe} si pour tous sommets $u,v \in A$, il existe un chemin entre $u$ et $v$. On note $u \rightarrow v$ s'il y a un chemin (peut être vide) de $u$ vers $v$, et $u\leftrightarrow v$ s'il y a $u \rightarrow v$ et $v \rightarrow u$. Le sous-graphe induit par les classes d'équivalence de $\leftrightarrow$ sont appelées \emph{composantes fortement connexes} de $G$. Autrement dit, une composante fortement connexe est un sous-ensemble de sommets tel qu’il est possible d'atteindre chacun des sommets à partir de l'autre.

Un graphe orienté est \emph{complet} si quels que soient deux sommets, il existe un arc les reliant dans un sens et un arc les reliant dans l'autre sens, c'est-à-dire, pour tous sommets $u,v \in V, (u,v) \in A$ et $(v,u) \in A$. Une \emph{clique} de $G$ est un ensemble de sommets $U \subseteq V$ tel que $G[U]$ est un graphe complet sans boucles, c'est-à-dire, pour tout sommet $u \in U, (u,u) \notin A$ et quels que soient $u,v \in U, (u,v) \in A$ tel que $u \neq v$.

Si $G=(V,E)$ un graphe non orienté, une \emph{couverture par sommets} de $G$ est un ensemble de sommets $U \subseteq V$ tel que chaque arrête de $G$ est incidente à au moins un sommet de $U$, c'est-à-dire, un sous-ensemble de sommets $U \subseteq V$ tel que pour chaque arrête $(u,v)\in E$ on a $u \in U$ ou $v \in U$. Une \emph{couverture minimale par sommets} est une couverture par sommets de taille minimale.

Les graphes sont très utilisés dans la résolution des problèmes. Cependant, en théorie des graphes, on rencontre plusieurs problèmes difficiles qui ne sont pas résolubles en temps polynomial (le problème de décider si un graphe contient un cycle hamiltonien, le problème de coloriage, le problème de la clique maximale, le problème de couverture minimale, le problème TCCM, le problème de la coupe maximum, etc.).
