\chapter{Énumération des transversaux à l'aide d'un arbre et/ou}\label{C:enumeration}

Une question naturelle issue du problème TCCM est la suivante : combien de transversaux de circuits de cardinalité minimale contient un graphe orienté de $n$ sommets? Clairement, il peut y en avoir un nombre exponentiel par rapport au nombre de sommets. Le graphe orienté $G= (V,A)$ à $n=|V|$ sommets de la figure \ref{enum} possède $2^{n/2}$ TCCM différents, chacun d'eux possède $n/2$ sommets. En effet, pour $i = 1,3,5,\ldots,n-3,n-1$, on peut inclure le sommet $v_i$ ou le sommet $v_{i+1}$ dans la solution de façon indépendante.

\begin{figure}[!h]
\centering
 \includegraphics [scale=0.9]{Logos/enum}
\caption[Un graphe ayant $2^{n/2}$ transversaux de circuits minimaux.]{Un graphe ayant $\bf{2^{n/2}}$ transversaux de circuits minimaux.}
\label{enum}
\end{figure}
L'énumération de tous les TCCM d'un graphe orienté $G=(V,A)$ est un problème NP-difficile. En 2002, Schwikowski et Speckenmeyer \cite{Schwikowski2002} ont présenté un algorithme permettant d'énumérer tous les TCCM dans un temps polynomial. En 2007, Formin et al. \cite{Fomin2007} ont démontré qu'il existe des graphes contenant un nombre exponentiel de TCCM et ont proposé un algorithme pour les énumérer en temps $O(1.8638^n)$. 

\section{Algorithme}

Lors du traitement de grandes familles d'ensembles de solutions, il devient rapidement impossible de stocker explicitement chacun des ensembles dans la mémoire. Par conséquent, on recourt souvent à des structures de données qui représentent implicitement ces ensembles. Dans notre algorithme, nous utilisons les arbres et/ou qui sont vus dans le chapitre précédent (voir chapitre \ref{C:arbres}) pour stocker implicitement la famille des TCCM.

Notre manière de faire pour énumérer l'ensemble de TCCM d'un graphe orienté consiste à diviser le problème en deux phases principales. La première phase permet de construire l'arbre et/ou représentant l'ensemble de TCCM du graphe et la deuxième phase se consacre au calcul de la famille de solutions (TCCM) à partir de l'arbre construit.

Vu que les DBD sont des structures populaires et très utilisées puisqu'ils offrent différents avantages que les arbres et/ou n'offrent pas (unicité de la représentation, compacité, etc.), nous ajoutons une autre phase qui permet de convertir l'arbre construit en un DBD et vice versa. 

Dans ce qu'il vient, nous décrivons notre algorithme et la manière de faire pour construire un arbre et/ou et énumérer la famille des TCCM d'un graphe orienté. 
    
\subsection{Construction de l'arbre et/ou représentant la famille de TCCM}
Cette section est consacrée à la description de l'algorithme qui calcule l'arbre et/ou représentant l'ensemble des TCCM d'un graphe orienté $G=(V,A)$. Tout d'abord, nous commençons par l'introduction de quelques définitions utiles dans la description de l'algorithme de construction.
\begin{defn}
Soit $G=(V,A)$ un graphe orienté, $u \in V$. On dit que $u$ est 
\begin{enumerate}
	\item essentiel s'il appartient à tous les TCCM de $G$;
	\item inutile s'il n'appartient à aucun TCCM de $G$.
\end{enumerate}
\end{defn}
Les sommets essentiels et les sommets inutiles peuvent être contractés sans compromettre l'espace des TCCM.
\begin{lemme} Soit $G=(V,A)$ un graphe orienté et $U \subseteq V$
\begin{enumerate}
  \item Pour tout sommet essentiel $u$, $U$ est un TCCM de $G$ si et seulement si $U-\{u\}$ est un TCCM de $G.$\del$(u)$;
	\item Pour tout sommet inutile $u$, $U$ est un TCCM de $G$ si et seulement si $U$ est un TCCM de $G.$\van$(u)$.
	\end{enumerate}
\end{lemme}
\dem 

\begin{enumerate}
	\item Soit $U \in TCCM(G)$, nous montrons que $U - \{u\}$ est un TCCM de $G.$\del$(u)$. Soit $c$ un circuit de $G$. Si $c$ est un circuit de $G.\del(u)$ donc, il est couvert par $U-\{u\}$; sinon, pour chaque TCCM de $G$, $c$ est couvert par $u$ parce que ce sommet est essentiel. 
	\item Soit $U \in TCCM(G)$, nous montrons que $U$ est un TCCM de $G.\van(u)$. Soit $c$ un circuit de $G.$\van$(u)$. Si $c$ est un circuit de $G$, il est donc couvert par $U$. Si $c$ n'est pas un circuit de $G$, il est donc forcément issu de l'application de l'opération $\van(u)$; et parce que $u$ n'appartient à aucun TCCM (c'est-à-dire, tout circuit $c$ passant par $u$ est couvert) donc $U$ est un TCCM de $G.$\van$(u)$.
\end{enumerate}



Un sommet possédant une boucle (self-loop) est nécessairement essentiel, tandis qu'un sommet supprimé par l'opérateur \iz\ ou \oz\ est nécessairement inutile (voir la figure \ref{domine}). Il n'est pas difficile de vérifier si un sommet est essentiel ou inutile (voir l'algorithme \ref{inutil}).
\begin{defn}
Soit $G=(V,A)$ un graphe orienté, $U$ un TCCM de $G$ et  $u$, $v \in V$. On dit que  
\begin{enumerate}
	\item $u$ est dominé par $v$, noté $u\preceq v$, si $u \in U$ implique que $(U-\{u\})\cup \{v\}$ est un TCCM de $G$;
	\item $u$ et $v$ sont équivalents, noté $u\equiv v$, si $u\preceq v$ et $v\preceq u$.
\end{enumerate}
\end{defn}
Il est facile de voir si deux sommets $u$ et $v$ sont équivalents, c'est-à-dire pour chaque TCCM $U$ de $G$, vérifier si $u \in U$ implique que $v \notin U$. Si $u$ et $v$ appartient à $U$, et parce que chaque circuit $c$ passant par $u$ passe aussi par $v$ donc $c$ est couvert par $U$ même si on enlève juste l'un des deux sommets $u$ ou $v$. Si on enlève les deux sommets, c'est une contradiction de la minimalité de $U$, donc $u$ et $v$ ne peuvent pas appartenir les deux à $U$. Ainsi, pour chaque $U \in TCCM(G)$, si $u \in U$ et $u\equiv v$, on peut remplacer $u$ par $v$ dans $U$ (voir la figure \ref{domine}). L’intérêt des sommets équivalents est qu'ils peuvent être fusionnés en un seul sommet. On a juste besoin de garder une trace des classes d'équivalence lors de la construction de l'arbre.
 
\begin{example} Dans la figure \ref{domine}, les sommets $\{2,10\}$ sont essentiels. Les sommets $\{1,3,7\}$ sont inutiles. Les trois sommets $\{4,5,6\}$ sont équivalents. Les deux sommets $8$ et $9$ sont équivalents et chacun d'eux domine l'autre. 
\end{example} 

\begin{figure}[!ht]
\centering
 \includegraphics [scale=0.8]{Logos/domine}
\caption{Exemple de sommets inutiles, essentiels, dominés et équivalents.}
\label{domine}
\end{figure}

\begin{lemme} Soit $G=(V,A)$ un graphe orienté et $u,v \in V$.
\begin{enumerate}
  \item Si $v$ est l'unique successeur de $u$, alors $u\preceq v$;
	\item Si $u$ est l'unique prédécesseur de $v$, alors $v\preceq u$;
	\item Si $u$ est l'unique prédécesseur de $v$ et $v$ est l'unique successeur de $u$, alors $u \equiv v$.
	\end{enumerate}
\end{lemme}
\dem Soit $U \in TCCM(G)$ et $u \in U$.
\begin{enumerate}
	\item Parce que chaque circuit passant par $u$ passe aussi par $v$ alors, on peut remplacer $u$ par $v$ dans $U$, donc $(U-\{u\})\cup \{v\}$ est un $TCCM(G)$. Par conséquent, $u\preceq v$.
	\item C'est l'énoncé symétrique de (1).
	\item Suit de (1) et (2).
\end{enumerate}

D'autres propriétés reliant la relation $\preceq$ avec les concepts de sommets essentiels et inutiles accélèrent l'algorithme de construction de l'arbre, en particulier lorsque les opérateurs \iu\ et \ou\ sont appliqués plusieurs fois. En effet, on peut déduire que certains sommets sont inutiles sans avoir à les vérifier explicitement.
\begin{lemme} Soit $G=(V,A)$ un graphe orienté et $u,v \in V$.
\begin{enumerate}
  \item Si $u\preceq v$ et $v$ est essentiel, alors $u$ est inutile; 
	\item Si $u\preceq v$ et $v$ est inutile, alors $u$ est inutile; 
	\end{enumerate}
\end{lemme}
\dem Soit $U \in TCCM(G)$ et $u \in U$.
\begin{enumerate}
	\item Si $v$ est essentiel donc $v \in U$ et parce que chaque circuit $c$ passant par $u$ passe aussi par $v$, alors $U \cup\{u\}$ est un TC de $G$. Contradiction de la minimalité de $U$.
	\item La définition de $\preceq$ implique que $(U-\{u\})\cup \{v\} \in TCCM(G)$, contradiction à la supposition que $v$ est inutile. 
\end{enumerate}
On est maintenant prêt à décrire notre algorithme de construction. Il prend en entrée un graphe orienté et renvoie un arbre et/ou représentant l'ensemble de TCCM. Dans l'algorithme \ref{calcul_arbre}, la fonction \nnode() permet de créer un nouveau nœud qui est peut être de type $et$, de type $ou$ ou une feuille. La stratégie de l'algorithme est comme suit :
\begin{enumerate}
\item Identifier les paires de sommets équivalents et les fusionner : Pour calculer les ensembles des sommets équivalents d'un graphe $G=(V,A)$, on commence par la construction du graphe représentant les paires de sommets dominés tel qu'il est décrit dans la fonction \domdig\ de l'algorithme \ref{domdi}.   
\begin{algorithm}[!ht]
\caption{Calcul du paires de sommets équivalents}
\label{domdi}
\begin{algorithmic}[1]
  \Function{\domdig}{$G:$ graphe orienté}: graphe
	\ForAll {$u \in A$}
	\If {$deg^-(u) = 1$ et $v$ est le prédécesseur de $u$}
	    \State $G'.\added (v,u)$
	\EndIf			
  \If {$deg^+(u) = 1$ et $w$ est le successeur de $u$}
	    \State $G'.\added (w,u)$
	\EndIf
	\EndFor
 \State \Return $G'$	
 \EndFunction
 \Statex
 \Function{\equivset}{$G:$ graphe orienté}: ensemble
  \State $dg \gets \domdig(G)$
  \State $\mathscr{C} \gets \scc(G)$
	\ForAll {$C \in \mathscr{C}$}
	\If {$|C| >= 2 $}
	    \State Ajouter $C$ à $S$
	\EndIf
 \EndFor				
 \State \Return $S$	
 \EndFunction
 \end{algorithmic}
 \end{algorithm}

Les sommets de chaque composante fortement connexe du graphe construit par la fonction \domdig\ constitue un ensemble de sommets équivalents. Si la classe d'équivalence du sommet $u$ est vide, on crée une feuille contenant $u$; sinon, on crée un sous-arbre enraciné par $\vee$ et les feuilles sont les sommets équivalents à $u$. 
\begin{figure}[!ht]
\centering
\includegraphics [scale=0.8]{Logos/equiv}
\caption[Représentation des sommets équivalents au sommet $u$ par l'arbre et/ou.]{Représentation des sommets équivalents au sommet $u$ par l'arbre et/ou.}
\label{equiv}
\end{figure}

Dans la figure \ref{equiv}, les sommets $v_1,v_2,\dots, v_n$ sont des sommets équivalents à $u$. On enlève $u$ du graphe $G$ et on continue à appliquer l'algorithme sur $G.\del(u)$. Il est important de garder une trace des sommets équivalents pour les étapes à venir. 


\item Identifier les sommets inutiles et les sommets essentiels : Pour calculer l'ensemble des sommets inutiles et les sommets essentiels d'un graphe $G=(V,A)$, on calcule un TCCM $U$ du graphe; puis pour chaque sommet $u$ de $U$, on vérifie s'il est essentiel en appliquant la fonction \isess; et pour chaque sommet $u$ de $V$ qui n'appartient pas à $U$, on vérifie s'il est inutile en appliquant la fonction \isinu\ (voir l'algorithme \ref{inutil}).
\begin{algorithm}[!ht]
\caption{Vérifier si un sommet est inutile ou essentiel}
\label{inutil}
\begin{algorithmic}[1]
  \Function{\isinu}{$G:$ graphe orienté, u:sommet}: booléen
	\If {$deg^-(u) = 0$  ou $deg^+(u) = 0$}
			\State \Return $Vrai$
	\EndIf			
  \State $G' \gets G.\del (u)$
  \State \Return {$|\onemfvs(G)|=|\onemfvs(G')|$}
 \EndFunction
\Statex
\Function{\isess}{$G:$ graphe orienté, u:sommet}: booléen
	\If {$(u,u) \in A $}
			\State \Return $Vrai$
	\EndIf			
  \State $G' \gets G.\van (u)$
  \State \Return {$|\onemfvs(G')|>|\onemfvs(G)|$}
 \EndFunction

 \end{algorithmic}
 \end{algorithm}

\item Appliquer l'opérateur \van\ sur les sommets inutiles;
\item Appliquer les opérateurs \pie\ et \dome\ ;
\item Éliminer les sommets essentiels et les ajouter à l'arbre et/ou. Comme on a déjà vu que le sous-ensemble des sommets essentiels appartient à chaque TCCM de $G$, donc ces sommets doivent être rassemblés par un nœud $et$ dans l'arbre et/ou. La figure \ref{essentiel} montre comment représenter le sous-ensemble de sommets essentiels dans l'arbre et/ou. $E=\{e_1,e_2, ...,e_k\}$ est le sous-ensemble de sommets essentiels;
\begin{figure}[!h]
\centering
\includegraphics [scale=0.8]{Logos/essentiel}
\caption[Représentation des sommets essentiels dans l'arbre et/ou.]{Représentation des sommets essentiels dans l'arbre et/ou.}
\label{essentiel}
\end{figure}
\item Si le graphe a plus d'une composante connexe, diviser le problème selon ces composantes. Les arbres représentant les TCCM de chaque composante sont rassemblés par un nœud de type $et$ (voir la figure \ref{composante}). 
\begin{figure}[!h]
\centering
\includegraphics [scale=0.8]{Logos/composante}
\caption[Division du problème selon ses composantes connexes.]{Division du problème selon ses composantes connexes.}
\label{composante}
\end{figure}
\item Sinon, choisir un sommet (celui qui a le degré maximum) et faire la séparation selon ce sommet (ce sommet doit être inclus ou exclu de la solution) (voir la figure \ref{branche}).
\begin{figure}[!h]
\centering
\includegraphics [scale=0.8]{Logos/branche}
\caption[Séparation et évaluation selon un sommet bien choisi.]{Séparation et évaluation selon un sommet bien choisi.}
\label{branche}
\end{figure}	
\end{enumerate}

\begin{algorithm}[!ht]
\caption{Calcul d’un arbre et/ou représentant tous les transversaux de circuits minimaux}
\label{calcul_arbre}
\begin{algorithmic}[1]
\Function{\gettree}{$G:$ graphe orienté, $id$:entier}: arbre et/ou 
 \State Fusionner toutes les paires de sommets équivalents 
 \State Appliquer l'opérateur \van\ sur tous les sommets inutiles 
 \State Appliquer les opérateur \pie\  et \dome
 \State Soit $E$ l’ensemble des sommets essentiels
 \If {$|E|>0$}  \Comment{Supprimer les sommets essentiels et les ajouter à l’arbre}
\algstore{myalg}
\end{algorithmic}
\end{algorithm}
\begin{algorithm}[!ht]                     
\begin{algorithmic} [1]                 
\algrestore{myalg}

		\State $id \gets id+1$
    \State $x \gets \nnode(id,\wedge, nul)$
		\ForAll {$u \in E$}
		    \State $id \gets id+1$
        \State Ajouter $\nnode(id,u.value)$ à $x.children$
    \EndFor
		\State $G' \gets G.\del(E)$
		\State Ajouter $\gettree(G')$ à $x.children$
 \Else
	  \State $\mathscr{C} \gets \cc(G)$
		\If {$|\mathscr{C}|>1$}  \Comment{Effectuer un branchement selon les composantes connexes}
     \State $id \gets id+1$ 
		 \State $x \gets \nnode(id,\vee, nul)$
		 \ForAll {$C \in \mathscr{C}$}
         \State Ajouter $\gettree(C)$ à $x.children$
     \EndFor
		\Else  \Comment{Effectuer un branchement en incluant/excluant un sommet quelconque}
		 \State Soit $u$ un sommet quelconque
		 \State $id \gets id+1$
		 \State $x \gets \nnode(id,\vee, nul)$
		 \State $id \gets id+1$
		 \State $y \gets \nnode(id,\wedge, nul)$
		 \State $id \gets id+1$
		 \State $y.children \gets \nnode(id,u.value)$
		 \State $G' \gets G.\del(u)$
		 \State Ajouter $\gettree(G')$ à $y.children$
		\State Ajouter $y$ à $x.children$
		\State $G' \gets G.\van(u)$
		\State Ajouter $\gettree(G')$ à $x.children$
		\EndIf
\EndIf
\State \Return $x$
 \EndFunction
 \end{algorithmic}
 \end{algorithm}


\subsection{Calcul de la famille des TCCM à partir de l'arbre et/ou}
La deuxième phase de notre application consiste à calculer la famille de TCCM (solutions) du graphe $G$ à partir de l'arbre et/ou. La section \ref{section321} et l'algorithme \ref{calcul_mfvs} décrivent comment calculer cette famille.
\begin{algorithm}[!ht]
\caption{Énumération de la famille de solutions à partir d'un arbre et/ou}
\label{calcul_mfvs}
\begin{algorithmic}[1]
\Function{\allmfvs}{$T:$ arbre et/ou}: List$<$List$<$value$>>$ 
    \State  \Return $\allmfvs(T.root)$
\EndFunction
\Statex
\Function{\allmfvs}{$v:$ Leaf}: List$<$List$<$value$>>$ \Comment{Si le nœud est une feuille}
     \State $Lsol \gets nul$
		 \State Ajouter $v.value$ à $Lsol$
		 \State  \Return $Lsol$
\EndFunction
\Statex
\Function{\allmfvs}{$v:$ OrNode}: List$<$List$<$value$>>$ \Comment{Si le nœud est de type $ou$} 
		 \ForAll {$w \in v.children$}
				  \State $sc \gets \allmfvs(w)$
					 \ForAll {$s \in sc$}
					     \State Ajouter $s$ à $Lsol$
		    	 \EndFor
     \EndFor
     \State  \Return $Lsol$
\EndFunction
\Statex
\Function{\allmfvs}{$v:$ AndNode}: List$<$List$<$value$>>$ \Comment{Si le nœud est de type $et$}
		 \ForAll {$w \in v.children$}
			   \State $sc \gets \allmfvs(w)$, $sol \gets Lsol$, $Lsol \gets nul$
				 \If {$sol \neq nul$}
				    \ForAll {$s_1\in sol$}
						    \ForAll {$s_2\in sc$}
								   \State Ajouter $s_1+s_2$ à $Lsol$ 
				        \EndFor
						\EndFor
				 \Else
						 \ForAll {$s_2\in sc$}
								   \State Ajouter $s_2$ à $Lsol$ 
				     \EndFor
		     \EndIf
   \EndFor
\State \Return $Lsol$
\EndFunction
\end{algorithmic}
\end{algorithm}

Les principaux avantages d'utiliser les arbres et/ou au lieu d'utiliser les DBD sont la compacité et la facilité de construction de la structure des arbres et/ou. 

En utilisant les arbres et/ou, on a pas besoin de chercher l'ordre optimal qui minimise le mieux la taille de la structure et qui est un problème NP-complet comme nous avons vu dans la section \ref{ordredbd}, et on a pas également besoin de faire la réduction de la structure car l'arbre construit est déjà optimal et aplati. Ainsi que la décomposition de l'ensemble de solution sur les variables est très simple par rapport aux DBD surtout quand le nombre de variables et supérieur à 2. Dans la figure \ref{comparais}, le DBD représentant l'ensemble des TCCM du graphe est composé de 9 nœuds et cela en utilisant l'ordre optimal qui est $c<d<a<b$ (le DBD est plus grand si on utilise un ordre autre que celui-ci) tandis que l'arbre est composé de 6 nœuds.    
\begin{figure}[!ht]
\centering
\subfigure[\label{compar-graphe}]{\includegraphics [scale=0.9]{Logos/compar-graphe}}
\hspace*{1cm}
\subfigure[\label{compar-dbd}]{\includegraphics [scale=0.8]{Logos/compar-dbd}}
\hspace*{1cm}
\subfigure[\label{compar-tree}]{\includegraphics [scale=0.9]{Logos/compar-tree}}
\caption[Comparaison entre un DBD et un arbre et/ou.]{Comparaison entre le DBD et l'arbre et/ou représentant la famille des TCCM d'un graphe. (a) Un graphe orienté. (b) Le DBD représentant les TCCM du graphe. (c) L'arbre et/ou représentant les TCCM du graphe.}
\label{comparais}
\end{figure}

Les procédures de conversion des arbres et/ou en DBD et des DBD en arbres et/ou ont été décrites et détaillées dans les sections \ref{section331}, \ref{section332}, pour plus de détails voir aussi les algorithmes \ref{flat_tree}, \ref{convert_to_dbd} et \ref{convert_to_tree}. 

\section{Implémentation}

Pour évaluer les différents algorithmes vus dans ce mémoire, notre choix s’est porté sur les langages de programmation \textbf{Java} et \textbf{Python}. L'algorithme qui permet de calculer l'arbre et/ou représentant la famille des TCCM a été implémenté en \textbf{Python 2.7}.  Naturellement, nous avons choisi le langage Python parce qu'il offre des modules facilitant la créations, l'utilisation, l'affichage et la manipulation des graphes. Grâce aux méthodes et fonctions offertes par le module \textbf{networkx-1.10-py2.7} de Python, nous avons pu implémenté les différents opérations de calcul et de réduction de notre algorithme. 

Le reste de l'application a été implémenté en utilisant le langage Java. Celui-ci a permis de mettre en place des interfaces homme-machine pour afficher nos résultats et intégrer les différents composants du système. Pour pouvoir sauvegarder, ouvrir et dessiner les arbres et/ou et les DBD créés (.dot, .txt, .png, etc) nous avons installé \textbf{Graphviz2.38} et l'avons intégré dans Java en ajoutant une classe \textbf{GraphViz.java}. Cette classe permet de gérer les interactions entre notre application Java et le programme \textbf{dot} de Graphviz2.38.

Dans notre application, il y a trois structures de données qui interviennent (les graphes orientés, les DBD et les arbres et/ou). La création des DBD et des arbres et/ou a été implémentée en Java, ainsi que les différentes opérations de calcul et de conversion permettant l’énumération et le stockage de la famille de TCCM.

La figure \ref{d_classes} représente le diagramme des classes intervenant dans notre application et les différentes relations et interactions entre elles, ainsi que les attributs et les méthodes de chaque classe. 
\begin{figure}[!ht]
\centering
\includegraphics[width=1\textwidth]{Logos/diagramme_classes}
\caption{Diagramme de classes}
\label{d_classes}
\end{figure}

Un arbre et/ou $T$ est défini par sa racine $T.root$ qui est de type $TNode$. Un TNode $v$ est de type \andnode$(id, \wedge, children)$, \ornode$(id, \vee, children)$ ou \leaf$(id,value)$ tel que $id$ est l'identifiant de $v$, $value$ est la valeur (\co) du $v$ et $children$ est la liste des nœuds enfants de $v$ qui sont de type TNode. Un DBD $D$ est défini par sa racine $D.root$ qui est de type $BNode$. Un BNode $w$ est de type \nterminalnode$(id,val,mask,low,high)$ ou \terminalnode$(id,val,mask)$ tel que $id$ est l'identifiant de $w$, $val$ est la variable de $w$, $mask$ un booléen utilisé pour désigner si le nœud est déjà parcouru pendant le calcul d'une fonction ou non, $low$ ($high$) est le fils bas (haut) d'un nœud non terminal qui est de type BNode. 

La famille des TCCM de $G$ est une liste de solutions, chaque solution (TCCM) est une liste de feuilles de $T$ et implémente l'interface \emph{Iterable} de Java. Pour l'énumération des solutions, nous avons implémenté des itérateurs pour chaque type de nœud de l'arbre. Cette façon de faire permet de garder juste une solution (la solution courante) dans la mémoire. Pour calculer la solution suivante, l'application teste les nœuds de la solution courante, si un nœud a une autre solution (hasNext()=vrai) alors on rend cette solution sinon on génère un nouvel itérateur. Nous avons introduit cette notion d'itérateurs pour ne pas occuper un grand espace mémoire par la famille de solution surtout quand elle est grande. Cette manière de faire permet de calculer rapidement la famille des TCCM même si le graphe est de grande taille.

L'application a été testée sur un ensemble de graphes générés aléatoirement. Les résultats des testes effectues seront affichés dans l'article publié pendant la session d'hiver 2016. 

Dans ce qui suite une liste de figures capturés de notre applications. La figure \ref{exmp_tree} est la capture d'écran de notre application montrant l'arbre et/ou représentant la famille des TCCM du graphe de la figure \ref{arbreetou}. L'arbre et/ou de la figure \ref{exmp-tree2_b} est l'arbre représentant la famille des TCCM du graphe de la figure \ref{exmp-tree2_a} produit par notre application. Cet arbre utilise 19 nœuds pour représenter une famille de 9 TCCM, chaque TCCM contient 4 nœuds. La figure \ref{toBDD} est la capture d'écran de notre application montrant un exemple de conversion d'un arbre et/ou en un DBD. La figure \ref{toTree} est une capture d'écran de notre application montrant un exemple de conversion d'un DBD en un arbre et/ou.


\begin{figure}[!ht]
\centering
\includegraphics[width=1\textwidth]{Logos/exmp_tree}
\caption[L'arbre représentant la famille des TCCM du graphe de la figure \ref{arbreetou}.]{L'arbre représentant la famille des TCCM du graphe de la figure \ref{arbreetou} généré par notre application.}
\label{exmp_tree}
\end{figure} 
\begin{figure}[!ht]
\centering
\subfigure[\label{exmp-tree2_a}]{\includegraphics [scale=0.8]{Logos/exmp-tree2_a}}
\hspace*{1.5cm}
\subfigure[\label{exmp-tree2_b}]{\includegraphics [scale=0.5]{Logos/exmp-tree2_b}}
\caption[L'arbre et/ou représentant la famille des TCCM d’un graphe.]{L'arbre et/ou produit par notre application représentant la famille des TCCM d’un graphe. (a) Un graphe orienté. (b) Un arbre et/ou représentant la famille des TCCM de ce graphe.}
\label{exmp-tree2}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[width=1\textwidth]{Logos/toBDD}
\caption{Le DBD correspondant à un arbre et/ou produit par notre application.}
\label{toBDD}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[width=1\textwidth]{Logos/toTree}
\caption{L'arbre et/ou correspond à un DBD produit par notre application.}
\label{toTree}
\end{figure}

\vspace*{10cm}
\section{Conclusion}
Dans ce chapitre, nous avons présenté notre algorithme permettant l’énumération des transversaux de circuits de cardinalité minimale d'un graphe orienté à l'aide d'un arbre et/ou et l'implémentation de celui-ci. En premier lieu, nous avons détaillé l'aspect théorique et la manière de faire pour construire l'arbre et/ou représentant la famille de solutions et comment calculer cette famille à partir de cet arbre. Ensuite, nous avons exposé l'implémentation de ces algorithmes et quelques exemples de tests générés par notre application.
