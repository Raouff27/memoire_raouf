\chapter{Arbres et/ou}\label{C:arbres}

Plusieurs algorithmes d'énumération qui reposent sur la technique classique de séparation et évaluation utilisent les DBD ou les DBDZ pour stocker les solutions combinatoires. Toutefois, ces structures ne sont pas toujours pratiques, surtout quand le branchement n'est pas binaire (plus de deux cas possibles à un branchement). Dans ce chapitre, nous introduisons une classe particulière d’arbres permettant l'énumération et l'enregistrement de manière compacte de larges familles d'ensembles de données. Nous allons donner son implémentation et étudier son efficacité dans le contexte d’énumération par la conception d'un algorithme résolvant le problème TCCM en utilisant cette structure.

Dans un premier temps, les arbres et/ou ont été introduits pour représenter graphiquement la réduction d'un problème à des conjonctions et disjonctions de sous-problèmes. Chaque nœud d'un arbre et/ou représente un sous-problème; en particulier, le nœud racine représente le problème initial à résoudre. Les nœuds ayant des nœuds enfants (ou successeurs) sont appelés \emph{nœuds non-terminaux}. Chaque nœud non terminal est soit de type \emph{et}, soit de type \emph{ou}. Une solution d'un problème dont le nœud correspondant est de type \emph{ou} est obtenue en récupérant la solution de l'un de ses successeurs, tandis qu'une solution à un problème dont le nœud sous-jacent est de type \emph{et} est obtenue en récupérant les solutions de tous ses nœuds successeurs. Les nœuds sans successeurs sont appelés \emph{terminaux} \cite{Kumar1983}. La figure \ref{aotree} représente l'arbre et/ou représentant la décomposition du problème $P$ en sous-problèmes. Le nœud $P$ est de type \emph{ou} et le nœud $Q$ est de type \emph{et}, les nœuds $P$ et $Q$ sont des nœuds non terminaux, les nœuds $R,S,T,U$ sont des nœuds terminaux. La résolution du $P$ est obtenue en récupérant la solution de $Q$ \emph{ou} $R$ \emph{ou} $S$. La résolution de $Q$ est obtenue en récupérant la solution de $T$ \emph{et} de $U$. Le problème $P$ se formule par $P= (Q \vee R \vee S)$ avec $Q= (T \wedge U)$ donc $P=((T \wedge U) \vee R \vee S)$. 
\begin{figure}[!ht]
\centering
\includegraphics [scale=1]{Logos/aotree}
\caption[Décomposition d'un problème $P$ par un arbre et/ou.]{Décomposition d'un problème $P$ par un arbre et/ou.}
\label{aotree}
\end{figure}

Plus tard, les arbres et/ou ont été utilisés comme une stratégie de recherche dans les espaces d'états et les espaces de recherche des jeu à deux personnes. Dans l'arbre d'un jeux à deux joueurs, les nœuds $ou$ représentent les mouvements d'un joueur, où il peut choisir lequel des mouvements à prendre et les nœuds $et$ représentent les mouvements de l'adversaire \cite{Kumar1983}. Les arbres et/ou sont également utilisés pour l'optimisation combinatoire dans les modèles graphiques \cite{Marinescu2009, Joo2014} et pour optimiser l'espace des contraintes dans les problèmes de satisfaction \cite{Marinescu2008}.
 
Dans le reste de ce chapitre, nous présentons la structure de base de la structure arbre et/ou que nous avons conçue, et nous discutons les différents algorithmes de manipulation de cette structure et les différentes opérations, requêtes et modifications qui peuvent être effectuées sur cette structure dans un contexte d'énumération.

\section{Structure de base}
Comme on a déjà vu, un arbre et/ou est un arbre dont lequel les nœuds internes sont des opérateurs logiques \emph{et} et \emph{ou} ($\wedge$, $\vee$) et les feuilles sont des valeurs de l'ensemble de solution. Pour chaque nœud interne de type \emph{ou}, la solution est obtenue en résolvant l'un de ses successeurs, tandis que la solution d'un nœud de type \emph{et} est obtenue en résolvant tous ses successeurs. 
\begin{example}
\label{famille}
Soit une famille d'ensembles \f$=\{\{0,1\},\{0,3\},\{1,2\}\}$. La figure \ref{arbreetou3} montre la représentation de cette famille par un DBD, DBDZ et un arbre et/ou selon l'ordre $0<1<2<3$. 

\begin{figure}[!h]
\centering
\subfigure[\label{arbre1}]{\includegraphics[scale=0.8] {Logos/arbre1}}
\hspace*{1.5cm}
\subfigure[\label{arbre2}]{\includegraphics[scale=0.8] {Logos/arbre2}}
\hspace*{1.5cm}
\subfigure[\label{arbre3}]{\includegraphics[scale=0.8] {Logos/arbre3}}
\caption[Représentation de la famille \f$=\{\{0,1\},\{0,3\},\{1,2\}\}$ par DBD, DBDZ et arbre et/ou.]{Représentation de la famille \f=\{\{0,1\},\{0,3\},\{1,2\}\} par DBD, DBDZ et arbre et/ou. (a) Représentation de \f\ par un DBD. (b) Représentation de \f\ par un DBDZ. (c) Représentation de \f\ par un arbre et/ou.}
\label{arbreetou3}
\end{figure}
Dans \ref{arbre1} et \ref{arbre2}, les arcs pointillés sortant d’un nœud étiqueté par la variable $v$ signifient que la variable $v$ prend la valeur 0 et donc qu'elle est exclue de l'ensemble de solution. Les arcs pleins signifient que la variable $v$ prend la valeur 1 et donc elle est incluse dans l'ensemble de solution. Un ensemble $E$ appartient à \f s’il existe un chemin partant de la racine vers la feuille $1$ en traversant les arcs pointillés ou les arcs pleins. Dans \ref{arbre3}, pour calculer la famille des ensembles, on part de la racine vers les feuilles, si le nœud est étiqueté par $\vee$, on récupère la solution de l'un de ses enfants, s'il est étiqueté par $\wedge$, on récupère la solution de tous ses enfants, si le nœud est une feuille, on l'ajoute à l'ensemble de solution courant. 
\end{example}

\begin{defn}
Soit $S$ un ensemble fini, $T$ un arbre et $V$ l'ensemble des nœuds de l'arbre $T$. Chaque ensemble \f$\subseteq 2^S$ est appelé \emph{famille} dans $S$, c'est-à-dire \f\:est une collection de sous-ensembles de $S$. Généralement, $|S|$ est petite, mais $|$\f$|$ pourrait être très grande. L'arbre $T$ est un arbre et/ou s'il existe trois fonctions \co: $V \rightarrow S \cup \{\wedge, \vee\}$, \va: $V \rightarrow 2^S$ et \fa: $V \rightarrow 2^{2^S}$ de telle sorte que les conditions suivantes soient vérifiées.
\begin{enumerate}
	\item Chaque feuille $v$ a un contenu dénoté par $\co(v) \in S$;
	\item Pour chaque nœud interne $v$, \co$(v)=\wedge$ ou \co $(v)=\vee$; 
	\item \label{point3} Pour chaque nœud $v \in V$,\\
    $\hspace*{1.7cm} \va(v) = \left\{
    \begin{array}{ll}
    \hspace*{-0.15cm} \{\co $(v)$\} & \mbox{si $v$ est une feuille;} \vspace*{-0.25cm} \\
    \hspace*{-0.15cm} \bigcup_{w \in v.fils}{\va(w)} & \mbox{si $v$ est un nœud interne.}
    \end{array}
    \right.$
  \item \label{point4} Pour chaque nœud $v \in V$,\\
    $\hspace*{1.7cm} \fa(v) = \left\{
    \begin{array}{lll}
    \hspace*{-0.15cm} \{\{\co $(v)$\}\} & \mbox{si $v$ est une feuille;} \vspace*{-0.15cm} \\
		\hspace*{-0.15cm} \bigcup_{w \in v.fils}{\fa(w)}  & \mbox{si \co($v$) = $\vee$;} \vspace*{-0.15cm} \\
    \hspace*{-0.15cm} \prod_{w \in v.fils} \fa(w) & \mbox{si \co($v$) = $\wedge$.}
    \end{array}
    \right.$\vspace*{0.4cm} \\
		où le produit $\prod$ de deux familles $\mathcal{F}$ et $\mathcal{G}$ est défini par $\mathcal{F} \cdot \mathcal{G}$ = \{$S \cup T | S \in \mathcal{F}, T \in \mathcal{G}$\}.
	\item Pour tout noeud $v$ tel que \co$(v)= \vee$ et $w$, $w' \in v.fils$, la condition \fa$(w) \cap \fa(w') \neq \emptyset$ implique $w = w'$.
	\item Pour tout noeud $v$ tel que \co$(v)= \wedge$ et $w$, $w' \in v.fils$, la condition \va$(w) \cap \va(w') \neq \emptyset$ implique $w = w'$.
\end{enumerate}
\end{defn}
Le point (1) est pour dénoter la valeur (clé) des nœuds feuilles. Le point (2) pour dénoter la valeur (clé) des nœuds internes ($\wedge , \vee$). Le point (3) indique que $\va(v)$ est l’ensemble de toutes les valeurs qui apparaissent dans au moins une feuille du sous-arbre induit par $v$. Le point (4) indique comment calculer récursivement, pour chaque nœud $v$, la famille des ensembles de toutes les solutions représentées par le sous-arbre dont la racine est $v$. Finalement, les conditions (5) et (6) imposent certaines restrictions sur le type d’arbre qui peut être manipulé.
Dans le point (5), on veut s’assurer que tout ensemble apparaissant dans une branche ne peut apparaître dans une autre (par conséquent, chaque ensemble apparaît une seule fois dans l’arbre). Quant au point (6), il permet d’optimiser l’utilisation des nœuds de contenu $\wedge$ puisqu’il interdit le fait qu’une clé puisse apparaître dans différentes branches.
\begin{example}
Soit la même famille  \f$=\{\{0,1\},\{0,3\},\{1,2\}\}$ de l'exemple \ref{famille}. L'arbre et/ou de la figure \ref{fonct} fournit une représentation de \co, \va\ et \fa\ de \f. Pour des raisons de lisibilité, nous utilisons souvent le dessin correspondant à \co\ uniquement.
\begin{figure}[!ht]
\centering
\subfigure[\label{content}]{\includegraphics[scale=0.8] {Logos/content}}
\hspace*{0.5cm}
\subfigure[\label{values}]{\includegraphics[scale=0.8] {Logos/values}}
\hspace*{0.5cm}
\subfigure[\label{family}]{\includegraphics[scale=0.8] {Logos/family}}
\caption[Arbres représentant \co, \va\ et \fa.]{Arbres représentant \co, \va\ et \fa\ de la famille \f $=\{\{0,1\},\{0,3\},\{1,2\}\}$. (a) L'arbre et/ou représentant la famille \f, Pour chaque nœud $v$, on écrit la valeur \co$(v)$ à l'intérieur du nœud. (b) Le même arbre et/ou avec \va$(v)$ à la place du \co$(v)$ pour chaque nœud $v$. (c) Le même arbre et/ou avec \fa$(v)$ à la place du \co$(v)$ pour chaque nœud $v$.}
\label{fonct}
\end{figure}
\end{example}
\vspace*{-0.6cm}
\begin{defn}
Un arbre et/ou $T$ est appelé \emph{k-uniforme} si $|E|= k$ pour chaque $E \in$ \fa$(r)$, où $r$ est la racine de $T$. Si $T$ est k-uniforme, l'ordre de $T$, noté \ordre$(T)$, est le nombre $k$.
\end{defn}
Dans de nombreuses situations, on se concentre à l'étude des familles d'ensembles ayant une taille constante. Une famille \f\ est appelé \emph{k-uniforme} (ou simplement uniforme si la valeur de $k$ est sans importance) si $|E|= k$ pour chaque $E \in$\f. L'arbre de la figure \ref{fonct} représente une famille $2$-uniforme.

\section{Opérations sur les arbres et/ou}
La plupart des opérations de base et les opérations avancées appliquées sur les arbres et/ou peuvent être définies de manière récursive par un parcours en profondeur de la racine vers les feuilles. Pour chaque nœud de l'arbre actuellement visité, les actions appliquées se diffèrent selon le type du nœud et l'opération à effectuer. Dans ce qui suit, nous présentons quelques uns des requêtes et modificateurs les plus intéressants effectués sur les arbres et/ou.

\subsection{Requêtes}

En général, les requêtes sont des opérations utilisées pour récupérer des informations à partir d'une structure de données sans la modifier. Nous ne pouvons pas énumérer toutes les requêtes possibles qui peuvent être effectuées sur les arbres et/ou en raison du manque d'espace, mais nous limitons notre étude à celles qui nous seront le plus utiles. Soient $T$ un arbre et/ou, $root(T)$ est la racine de $T$, \f une famille représentée par l'arbre $T$ définie sur un ensemble fini de valeurs $S$, nous sommes intéressés aux requêtes suivantes : 
\begin{enumerate}
  \item Calculer l'ensemble de valeurs de $S$ à partir de l'arbre $T$ (\va($T$));
	\item Calculer la famille \f représentée par un arbre $T$ (\Family($T$));
	\item Calculer la cardinalité (le nombre d'ensembles) de la famille \f (\Card(\f));
	\item Vérifier si un ensemble donné $E$ est appartient à la famille \f ($E \in $ \f);
	\item Calculer un ensemble aléatoire $E$ avec une probabilité uniforme (\Random(\f));
  \item Vérifier si l'arbre est uniforme (\IsUniform(\f)).
\end{enumerate}
\label{section321}
Pour calculer \va($T$) et \Family($T$), on procède tel qu'il est décrit dans les point \ref{point3} et \ref{point4} de la définition de la structure de base des arbres et/ou. L'ensemble de valeurs $S$ et la famille de solutions \f de l'arbre $T$ sont égaux à l'ensemble de valeurs et la famille de solutions du nœud racine de $T$. Pour plus de détails sur le calcul du \Family($T$), voir l'algorithme \ref{calcul_mfvs}.
 
Pour calculer \Card(\f), il suffit de définir de manière récursive \Card\ des nœuds feuilles, des nœuds $\wedge$ et des nœuds $\vee$ en montant jusqu'à la racine de l'arbre. Ainsi, \Card(\f) = \Card($r$), où $r$ est la racine de $T$. \Card\ est définie récursivement par 

$ \Card($v$) = 
\left\{
\begin{array}{ll}
1 & \mbox{si $v$ est une feuille;} \\
\sum_{w\in v.fils} \Card(w) & \mbox{si $v$ est de type $ou$;}\\
\prod_{w\in v.fils} \Card(w) & \mbox{si $v$ est de type $et$.}
\end{array}\right.
$

\begin{comment}
$$ \Card($v$) = 
\left\{
\begin{array}{ll}
1 & \mbox{si $v$ est une feuille;} \\
\sum_{w\in v.fils} \Card(w) & \mbox{si $v$ est de type $ou$;}\\
\prod_{w\in v.fils} \Card(w) & \mbox{si $v$ est de type $et$.}
\end{array}\right.
$$
\end{comment}
Pour vérifier si un ensemble $E$ est appartient à la famille \f, on procède comme suit. Pour chaque nœud $v$: 
\begin{enumerate}
	\item Si $v$ est une feuille, retourner vrai si $E=\{v\}$;
	\item Si $v$ est un nœud de type \emph{ou}, retourner vrai si et seulement si $E$ appartient à au moins une famille induite par un nœud enfant de $v$;
	\item Si $v$ est un nœud de type \emph{et}, pour chaque nœud enfant $w$ de $v$, vérifier si $E \cap \va (w)$ appartient à la famille induite par le sous-arbre enraciné par $w$ (si c'est le cas, retourner vrai, sinon, retourner faux).
\end{enumerate}

Pour extraire un ensemble aléatoire en garantissant que tous les ensembles ont la même probabilité d’être choisis, on introduit pour chaque nœud $v$ de l'arbre une probabilité uniforme dépend de \Card(v) et \Card(\f). Dans la figure \ref{content}, le sous-arbre gauche de la racine représente deux ensembles et le sous-arbre droit représente un seul ensemble. Afin que le calcul soit équitable, on introduit la probabilité $\frac{2}{3}$ pour le sous-arbre gauche et $\frac{1}{3}$ pour le sous-arbre droit. Pour calculer $E$, il suffit de suivre les étapes suivantes sur chaque nœud $v$.
\begin{enumerate}
	\item Si $v$ est une feuille, retourner $\{\co (v)\}$;
	\item Si $v$ est un nœud de type \emph{et}, retourner $\bigcup_{w \in v.fils}\Random(w)$;
	\item Si $v$ est un nœud de type \emph{ou}, choisissez un nœud enfant $w$ de $v$ avec une probabilité de $\Card(w)/\Card(v)$ pour chaque enfant de $v$, puis retourner $\Random(w)$.
\end{enumerate}

Pour vérifier que \f est uniforme, on procède comme suite, Soit $k$ un entier et $v$ un nœud de $T$. $\IsUniform(v,k)$ est défini comme suit:
\begin{enumerate}
	\item Si $v$ est une feuille, $\IsUniform(v,k)$ est vrai si et seulement si $k=1$;
	\item Si $v$ est un nœud de type \emph{ou}, $\IsUniform(v,k)$ est vrai si et seulement si $\IsUniform(w,k)$ pour chaque nœud enfant $w$ de $v$;
	\item Si $v$ est un nœud de type \emph{et}, $\IsUniform(v,k)$ est vrai si et seulement si $\IsUniform(w,k_w)$ pour chaque nœud enfant du nœud $v$ et un entier $k_w$ avec $k = \sum_{w\in v.fils} k_w $.
\end{enumerate}

\begin{thm}\label{thm6}
Soit $T$ un arbre et/ou de $n$ nœuds. Les opérations \Card, \Random, $\in$, \IsUniform\ ont une complexité de $O(n)$.
\end{thm}
\dem
\Card\ effectue un seul parcours de l'arbre et/ou, qui est trivialement de complexité $O(n)$. L'opération $\in$ est également linéaire, parce que $\va(v)$ est connue pour chaque nœud $v$ de $T$. Il suffit de stocker cette information directement dans la structure de données ou la pré-calculer en temps linéaire. \Random\ est également linéaire, car elle s'effectue en un seul parcours de l'arbre en utilisant \Card, ce qui est $O(n)$. Enfin, \IsUniform\ est clairement $O(n)$.  

\subsection{Modificateurs}

Plusieurs modificateurs peuvent être définis sur les arbres et/ou. Nous nous intéressons ici à l'aplatissement et à la binarisation de l'arbre~:
\begin{enumerate}
  \item Aplatir l'arbre $T$ (\Flat($T$));
	\item Binariser l'arbre $T$ (\Binarize($T$)).
\end{enumerate}
L'aplatissement d'un arbre et/ou peut être considéré comme un moyen de compression de données afin d'économiser l'espace mémoire occupé par la structure. Il est aussi un moyen d'obtenir une structure de données où les nœuds de type \emph{et} alternent avec les nœuds de type \emph{ou} à chaque niveau. Plus précisément, l'aplatissement effectue les opérations suivantes :
\begin{enumerate}
  \item Si un nœud interne $v$ n'a pas enfant, alors on le supprime;
	\item Si un nœud interne $v$ a un seul enfant $w$, alors on supprime $v$ et on le remplace par $w$;
	\item Si un nœud interne $v$ a un enfant $w$ du même type $(\co(v) =\co(w))$, alors on déplace tous les enfants du nœud $w$ à la liste des enfants du nœud $v$ et on supprime $w$ (voir l'algorithme \ref{flat_tree}).
	\end{enumerate}

\begin{algorithm}[!ht]
\caption{Aplatissement d'un arbre et/ou}
\label{flat_tree}
\begin{algorithmic}[1]
\Function{\Flat}{$T:$ arbre et/ou}: arbre et/ou 
    \State  \Return $\Flat(T.root)$
\EndFunction
\Statex
\Function{\Flat}{$v:$ Leaf}: arbre et/ou \Comment{Nœud feuille}
    \State  \Return $v$
\EndFunction
\Statex
\Function{\Flat}{$v:$ InternalNode}: arbre et/ou \Comment{Nœud interne} 
     \If {$|v.children|=0$}
		    \State Supprimer $v$
		 \Else
		 \If {$|v.children|=1$}
		    \State $v \gets \Flat(v.children[1])$
		 \Else
		    \ForAll {$w \in v.children$}
				   \If {$w.value= v.value$}
					     \ForAll {$z \in w.children$}
					    \State Ajouter $\Flat(z)$ à $v.children$
							 \EndFor
					 \State Supprimer $w$
\algstore{myalg}
\end{algorithmic}
\end{algorithm}
\begin{algorithm}                     
\begin{algorithmic} [1]                 
\algrestore{myalg}
					 \Else
					     \State $w \gets \Flat(w)$
					 \EndIf
				\EndFor				
		 \EndIf
		\EndIf
  \State  \Return $v$
\EndFunction
\end{algorithmic}
\end{algorithm}

\begin{example} La figure \ref{example_flat}, montre l'aplatissement d'un arbre représentant la famille \f=$\{\{1,4\},\{2,4\},\{3,4\}\}$. 
\end{example}
\begin{figure}[!ht]
\centering
\subfigure[]{\includegraphics[width=0.3\textwidth]{Logos/flat1} \label{flat1}}
\hspace*{3cm}
\subfigure[]{\includegraphics[width=0.25\textwidth]{Logos/flat2}\label{flat2}}
\caption{Aplatissement d'un arbre et/ou.}
\label{example_flat}
\end{figure}


Inversement, l'opération \Binarize\ permet à un nœud interne d'avoir au plus deux nœuds enfants. Cela se fait par un parcours de la racine vers les feuilles. Si le nœud courant a un seul enfant, on le remplace par son enfant unique et on continue la binarisation, sinon, si le nœud courant possède plus de deux nœuds enfants, on insère autant de nœuds internes de même type que le nœud courant selon les besoins. La binarisation de l'arbre simplifie la mise en œuvre de tous les opérations définies sur les arbre et/ou mais donne un arbre plus grand en taille donc elle occupe plus d'espace mémoire.
\begin{example} La figure \ref{example_bin}, montre la binarisation d'un arbre représentant la famille \f=$\{\{1\},\{2,3\},\{4,5,6\}\}$. 
\end{example}
\begin{figure}[!ht]
\centering
\subfigure[]{\includegraphics[width=0.35\textwidth]{Logos/binarize1} \label{binarize1}}
\hspace*{3cm}
\subfigure[]{\includegraphics[width=0.25\textwidth]{Logos/binarize2}\label{binarize2}}
\caption{Binarisation d'un arbre et/ou.}
\label{example_bin}
\end{figure}
\begin{thm}\label{thm6}
Soit $T$ un arbre et/ou de $n$ nœuds. Les opérations \Flat($T$) et \Binarize($T$) ont une complexité de $O(n)$ dans le pire cas.
\end{thm}
\dem
Il est clair que les opérations \Flat\ et \Binarize\ s'effectuent en un seul parcours de l'arbre. Pour chaque nœud $v$, \Flat\ supprime $v$ s'il n'a pas d'enfants ou elle le supprime et déplace ses enfants vers la liste des enfants de son père. La suppression et le déplacement s'effectuent en temps constant ($O(1)$). Donc \Flat\ se réalise en $O(n)$. \Binarize\ effectue l'insertion d'un nouveau nœud ($O(1)$) et le déplacement des nœuds enfants du nœud courant vers la liste des enfant du nouveau nœud créé qui sont dans le pire cas $n$ déplacements, donc en total \Binarize\ se fait en $O(n)$. 

Plusieurs autres opérations de vérification et de recherche peuvent être définies sur les arbres et/ou comme la recherche d'une clé dans l'arbre, la recherche des sous-ensembles de solutions et des sous-familles, le calcul de la famille de solution d'un nœud interne, l'extraction des sous-familles qui ne contient certains valeurs, etc. Ainsi que d'autres opérations ensemblistes comme les opérations d’union, d’intersection, de complétude et de différence peuvent être aussi définis sur les arbres et/ou, ces dernières opérations est de préférable les appliquer sur les DBD et DBDZ correspondants aux arbres et/ou car il est facile de convertir un arbre et/ou en un DBD ou un DBDZ comme nous le voyons dans la section suivante. 

\section{Relation avec les diagrammes binaires de décision}
Dans cette section on s'intéresse au lien entre les arbres et/ou et les DBD/DBDZ en expliquant les conversions effectuées pour transformer un arbre et/ou en un DBD/DBDZ et vice versa. Notant que dans un contexte d’énumération, le fait d'avoir des négations de valeurs dans les feuilles de l'arbre et/ou et ainsi dans le DBD correspondant n'a aucun sens, on ne trouve donc pas d'arcs indexés par $1$ ($0$) allant d'un nœud interne vers le nœud 0-terminal (1-terminal) dans le DBD.

\subsection{Conversion d'un arbre et/ou en un diagramme binaire de décision} \label{section331}
Pour construire le DBD/DBDZ correspondant à un arbre et/ou, il suffit de parcourir l'arbre de la racine vers les nœuds feuilles en construisant récursivement le DBD correspondant à chaque nœud actuellement visité selon son type. Si le nœud courant est une feuille, le DBD correspondant est composé d'un seul nœud interne étiqueté par la valeur de la feuille et ayant le nœud 0-terminal comme fils bas et le nœud 1-terminal comme fils haut (voir figure \ref{tree-bdd1}). Si le nœud courant est un nœud interne, on construit récursivement les DBD correspondants à ses enfants puis on applique récursivement la fonction \Apply\ (voir la fonction \ref{apply}) sur ces DBD pour avoir le DBD correspondant au nœud courant (voir section \ref{ope_log}). L'opérateur logique appliqué à chaque étape est selon le type du nœud, s'il est de type \emph{et}, on applique l'opérateur logique $\wedge$ sinon on applique l'opérateur logique $\vee$. Si le nœud courant est de type \emph{et}, alors les DBD correspondants à ses enfants sont liés par des arcs indexés par $1$ ou des arcs pleins (voir la figure \ref{tree-bdd2}), sinon ils sont liés par des arcs indexés par $0$ ou des arcs pointillés (voir la figure \ref{tree-bdd3}). En pratique la procédure de construction du DBD correspondant à un arbre et/ou commence par la construction des DBD correspondants aux feuilles puis elle remonte jusqu'à la racine de l'arbre. L'algorithme \ref{convert_to_dbd} résume le processus de conversion, la fonction \Apply\ est inspirée de la fonction \Apply\ définie par Bryant, elle traite juste les deux opérateurs $et$ et $ou$ dans le cas où il n'y a pas de négation. Cette fonction redéfinie permet d'optimiser le temps d’exécution et donne un DBD ordonné et réduit.  
\begin{figure}[!ht]
\centering
\subfigure[]{\includegraphics[width=0.17\textwidth]{Logos/tree-bdd1} \label{tree-bdd1}}
\hspace*{0.5cm}
\subfigure[]{\includegraphics[width=0.35\textwidth]{Logos/tree-bdd2}\label{tree-bdd2}}
\hspace*{0.5cm}
\subfigure[]{\includegraphics[width=0.35\textwidth]{Logos/tree-bdd3}\label{tree-bdd3}}
\caption[Conversion d'un arbre et/ou en un DBD.]{Conversion d'un arbre et/ou en un DBD. (a) Le DBD correspondant à un nœud feuille. (b) Le DBD correspondant à un nœud de type et. (c) Le DBD correspondant à un nœud de type ou.}
\end{figure}
\vspace*{-0.3cm}
\begin{algorithm}[!ht]
\caption{Convertir un arbre et/ou en un DBD}
\label{convert_to_dbd}
\begin{algorithmic}[1]
\Function{\ctd}{$T:$ arbre et/ou, $id:entier,zero,un:DBD$}: DBD 
    \State  $zero \gets \ntn(0,$"0"$,false)$
		\State  $un \gets \ntn(1,$"1"$,false)$
    \State  \Return $\ctd(T.root,2,zero,un)$
\EndFunction
\Statex
\Function{\ctd}{$v:$ Leaf, $id:entier,zero,un:DBD$}: DBD 
     		 \State  $dbd \gets \nntn(id,v.value,false,zero,un)$
		 \State  \Return $dbd$
\EndFunction
\Statex
\Function{\ctd}{$v:$ OrNode, $id:entier,zero,un:DBD$}: DBD 
		 \State $dbd1 \gets \ctd(v.children[1],id,zero,un)$
		 \State $dbd \gets dbd1$
\algstore{myalg}
\end{algorithmic}
\end{algorithm}
\begin{algorithm}[!ht]                     
\begin{algorithmic} [1]                 
\algrestore{myalg}
		 \State $id \gets dbd.root.id+1$
		 \For {$i \gets 2$ \textbf{à} $|v.children|$}
				  \State $dbd2 \gets \ctd(v.children[i],id,zero,un)$
					\State $dbd \gets \Apply (dbd1, dbd2, \vee)$
          \State $id \gets dbd.root.id+1$		
          \State $dbd1 \gets dbd$
     \EndFor
     \State  \Return $dbd$
\EndFunction
\Statex

\Function{\ctd}{$v:$ AndNode, $id:entier,zero,un:DBD$}: DBD 
		 \State $dbd1 \gets \ctd(v.children[1],id,zero,un)$
		 \State $dbd \gets dbd1$
		\State $id \gets dbd.root.id+1$
		 \For {$i \gets 2$ \textbf{à} $|v.children|$}
				  \State $dbd2 \gets \ctd(v.children[i],id,zero,un)$
					\State $dbd \gets \Apply (dbd1, dbd2, \wedge)$
					\State $id \gets dbd.root.id+1$
          \State $dbd1 \gets dbd$
     \EndFor
     \State  \Return $dbd$
\EndFunction
\end{algorithmic}
\end{algorithm}

\begin{example} La figure \ref{ex_tree_bdd} représente les différentes étapes à suivre pour convertir l'arbre et/ou représentant la famille \f $=\{\{1,4\},\{2,3,4\}\}$ en un DBDOR.
\vspace*{-0.2cm}
\begin{figure}[!ht]
\centering
\subfigure[]{\includegraphics[width=0.18\textwidth]{Logos/ex_tree_bdd1} \label{ex_tree_bdd1}}
\hspace*{1cm}
\subfigure[]{\includegraphics[width=0.075\textwidth]{Logos/ex_tree_bdd2}\label{ex_tree_bdd2}}
\hspace*{1cm}
\subfigure[]{\includegraphics[width=0.1\textwidth]{Logos/ex_tree_bdd3}\label{ex_tree_bdd3}}
\hspace*{1cm}
\subfigure[]{\includegraphics[width=0.14\textwidth]{Logos/ex_tree_bdd5}\label{ex_tree_bdd5}}
\caption[Conversion de l'arbre et/ou représentant la famille \f $=\{\{1,4\},\{2,3,4\}\}$ en un DBDOR.]{Conversion de l'arbre et/ou représentant la famille \f$=\{\{1,4\},\{2,3,4\}\}$ en un DBDOR. (a) L'arbre et/ou. (b) Les DBD correspondant aux feuilles. (c) Le DBD correspondant à $(2 \wedge 3)$ et le DBD correspondant à $(1 \vee (2 \wedge 3))$. (d) Le DBDOR représentant \f.}
\label{ex_tree_bdd}
\end{figure}
\end{example}

\vspace*{-0.4cm}
\begin{thm}\label{thm7}
Soit $T$ un arbre et/ou de $n$ nœuds et $D$ le DBDOR correspondant à $T$. L'opération de conversion de l'arbre $T$ en un DBDOR a une complexité de $O(n^3)$.
\end{thm}
\dem Le parcours de l'arbre est d'ordre $O(n)$, la fonction \Apply\ est d'ordre $O(n^2)$ donc la conversion de l'arbre $T$ en un DBDOR est d'ordre $O(n^3)$. 

\subsection{Conversion d'un diagramme binaire de décision en un arbre et/ou} \label{section332}

Pour convertir un DBD en un arbre et/ou, il suffit de parcourir le DBD de la racine vers les nœuds terminaux en ajoutant récursivement des nœuds internes ($\wedge$ et $\vee$) et en faisant des liaisons entre l'arbre correspondant au nœud courant et les arbres correspondants à ses fils bas et haut. L'arbre correspondant à un nœud interne dépend du type de ses nœuds fils, pour chaque nœud interne $v$ du DBD :
\begin{enumerate}
	\item Si son fils bas est le nœud 0-terminal et son fils haut est le nœud 1-terminal, l'arbre correspondant est une feuille de valeur $v$ (voir figure \ref{bdd-tree1});
	\item Si son fils bas est le nœud 0-terminal et son fils haut est un nœud de décision ayant $w$ comme variable, l'arbre et/ou correspondant est composé d'un nœud interne de type \emph{et} ayant la feuille $v$ et l'arbre correspondant au fils haut ($AOT(w)$: And/Or Tree (w). i.e. l'arbre et/ou de racine $w$) comme enfants (voir figure \ref{bdd-tree2});
	\item Si son fils haut est le nœud 1-terminal et son fils bas est un nœud de décision ayant $w$ comme variable, l'arbre et/ou correspondant est composé d'un nœud interne de type \emph{ou} ayant la feuille $v$ et l'arbre correspondant au fils bas ($AOT(w)$) comme enfants (voir figure \ref{bdd-tree3});
	\item Si son fils haut est un DBD de racine $u$ et son fils bas est un DBD de racine $w$, l'arbre et/ou correspondant est composé d'un nœud interne de type \emph{ou} ayant l'arbre correspondant au fils bas ($AOT(w)$) 
et un nœud de type \emph{et} comme enfants, ce nœud de type \emph{et} de son tour, a la feuille $v$ et l'arbre correspondant au fils bas ($AOT(u)$) comme enfants (voir figure \ref{bdd-tree4}).
\end{enumerate}

A la fin de l'opération de conversion, on applique la fonction d'aplatissement sur l'arbre résultant. L'algorithme \ref{convert_to_tree} résume le processus de conversion. La fonction $\isnt(v)$ est vraie si $v$ est un nœud non terminal, $\iszt(v)$ est vraie si $v$ est un nœud 0-terminal et $\isut(v)$ est vraie si $v$ est un nœud 1-terminal. 

\begin{figure}[!ht]
\centering
\subfigure[]{\includegraphics[width=0.25\textwidth]{Logos/bdd-tree1} \label{bdd-tree1}}
\hspace*{4cm}
\subfigure[]{\includegraphics[width=0.3\textwidth]{Logos/bdd-tree2}\label{bdd-tree2}}
\subfigure[]{\includegraphics[width=0.3\textwidth]{Logos/bdd-tree3}\label{bdd-tree3}}
\hspace*{3cm}
\subfigure[]{\includegraphics[width=0.44\textwidth]{Logos/bdd-tree4}\label{bdd-tree4}}
\caption[Les différents cas possibles pendant la conversion d'un DBD en un arbre et/ou.]{Les différents cas possibles pendant la conversion d'un DBD en un arbre et/ou. (a) Les fils de la variable sont des nœuds terminaux. (b) Le fils bas 0-terminal et le fils haut non terminal. (c) Le fils haut 1-terminal et le fils bas non terminal. (d) les deux fils non terminaux.}
\end{figure}

\begin{algorithm}[!ht]
\caption{Convertir un DBD en un arbre et/ou}
\label{convert_to_tree}
\begin{algorithmic}[1]
\Function{\cbt}{$D:$ DBD, $id$:entier}: arbre et/ou 
 \State $v \gets D.root$ 
 
 \If {$\isnt(v)$}
     \State $low \gets v.low$, $high \gets v.high$
		 \If {$\iszt(low)$ \textbf{et} $\isut(high)$}
					    \State $id \gets id+1$, $x \gets \nnode(id,v.value)$
		 \Else
					\If {$\isnt(high)$ \textbf{et} $\iszt(low)$}
					   \State $id \gets id+1$, $x \gets \nnode(id,\wedge,nul)$
						\State $id \gets id+1$, $y \gets \nnode(id,v.value)$
						 \State Ajouter $y$ à $x.children$
						\State Ajouter $\cbt(high)$ à $x.children$
			\Else
						 \If {$\isnt(low)$ \textbf{et} $\isut(high)$}
							  \State $id \gets id+1$, $x \gets \nnode(id,\vee,nul)$
						    \State $id \gets id+1$, $y \gets \nnode(id,v.value)$
						    \State Ajouter $y$ à $x.children$
						    \State Ajouter $\cbt(low)$ à $x.children$
									\Else
									  \State $id \gets id+1$, $x \gets \nnode(id,\vee,nul)$
						        \State $id \gets id+1$, $y \gets \nnode(id,\wedge,nul)$
										\State $id \gets id+1$, $z \gets \nnode(id,v.value)$
                    \State Ajouter $z$ à $y.children$
						        \State Ajouter $\cbt(high)$ à $y.children$
										\State Ajouter $y$ à $x.children$
						        \State Ajouter $\cbt(low)$ à $x.children$
									\EndIf
					\EndIf
     \EndIf
 \EndIf
 \State \Return $\Flat(x)$
\EndFunction
\end{algorithmic}
\end{algorithm}


\begin{example}
La figure \ref{ex_bdd_tree}, représente les différentes étapes à suivre pour convertir le DBD représentant la famille \f $=\{\{a,c,d\},\{b\},\{e\}\}$ en un arbre et/ou.
\vspace*{-0.2cm}
\begin{figure}[!ht]
\centering
\subfigure[]{\includegraphics[width=0.15\textwidth]{Logos/ex_bdd_tree1} \label{ex_tree_bdd1}}
\hspace*{2cm}
\subfigure[]{\includegraphics[width=0.265\textwidth]{Logos/ex_bdd_tree2}\label{ex_tree_bdd2}}
\hspace*{2cm}
\subfigure[]{\includegraphics[width=0.19\textwidth]{Logos/ex_bdd_tree3}\label{ex_tree_bdd3}}
\caption[Conversion du DBD représentant la famille \f $=\{\{a,c,d\},\{b\},\{e\}\}$ en un arbre et/ou.]{Conversion du DBD représentant la famille \f $=\{\{a,c,d\},\{b\},\{e\}\}$ en un arbre et/ou. (a) Le DBDZ représentant la famille \f. (b) Les arbres correspondants aux DBD représentants $e$, $d$, $(c\wedge d)$ et $(b\vee e)$. (c) En haut, l'arbre et/ou représentant \f\ avant l'aplatissement, en bas, l'arbre et/ou représentant \f\ après l'aplatissement.}
\label{ex_bdd_tree}
\end{figure}
\end{example}
\begin{thm}\label{thm6}
Soit $D$ un DBD de $n$ nœuds et $T$ l'arbre et/ou correspondant à $D$ ayant $m$ nœuds. L'opération de transfert de $D$ à un arbre et/ou a une complexité de $O(n+m)$.
\end{thm}
\dem La construction de l'arbre et/ou se fait par un parcours du DBD qui est d'ordre $O(n)$, et l'aplatissement s'effectue en $O(m)$, donc le tout en $O(n+m)$

Les arbres et/ou peuvent représenter n'importe quelle famille d'ensemble de solutions. Ils produisent des représentations plus compactes et plus expressives que les DBD et les DBDZ dans certaines situations. Sa construction semble complexe pour représenter une petite famille d'ensembles, mais elle est une alternative compacte lors du calcul de grandes familles, en particulier lorsque les nœuds $\wedge$ partitionnent efficacement l'ensemble des solutions. L'utilisation de cette structure de données semble nouvelle et d'après nos premières observations, elle pourrait être utilisée dans plusieurs contextes.

\section{Conclusion}

Les arbres et/ou offrent un modèle de représentation alternatif et compact pour stocker les familles de solutions combinatoires. Ils peuvent être utilisés comme structure pour enregistrer et manipuler les ensembles de solutions des différents problèmes d’énumération, surtout, parce que la plupart des opérations et modifications appliquées sur ceux-ci s’effectuent en temps polynomial.
