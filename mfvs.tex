\chapter{Transversaux de circuits}\label{C:tccm}

Le problème d'énumérer les transversaux de circuits de cardinalité minimale (TCCM) dans un graphe orienté est un problème d'optimisation combinatoire fondamental de la classe NP-difficile. Il existe une version pour les graphes orientés et les graphes non orientés, toutes les deux étant NP-difficiles. D'autres variations possibles consistent à ajouter des poids sur les sommets ou les arcs/arêtes du graphe et de chercher un ou plusieurs transversaux de poids minimal \cite{Festa1999}. Dans ce chapitre, nous nous intéressons seulement aux graphes orientés non pondérés dans le but de calculer un transversal de cardinalité minimale. 

Depuis la définition du problème TCCM par Karp et la démonstration qu'il est NP-difficile \cite{Yannakakis1978}, le problème TCCM est largement étudié et plusieurs approches ont été proposées afin de trouver des algorithmes optimaux permettant de le résoudre.

Au début, l’étude du problème est limitée à des classes particulières de graphes. Plusieurs algorithmes polynomiaux ont été proposés en ce sens \cite{Smith1975,Shamir1979,Levy1988,Lin2000}. Plus tard, quelques chercheurs se sont intéressés à l'étude du problème dans le cadre général \cite{Razgon2006,Fomin2007}. Cependant, sans surprise, tous les algorithmes proposés dans le cas général sont exponentiels en la taille du graphe.

Dans ce chapitre, nous étudions en détails l'approche proposée par Levy et Low et étendue par Lin et Jou; qui permet de calculer un TCCM en temps polynomial pour certains types de graphes appelés \emph{DOME-contractibles}. Mais avant d'entrer dans les détails de cette approche, on démontre d'abord la NP-complétude de la version décisionnelle du problème TCCM.

\section{Complexité du problème}

Le problème TCCM est un problème NP-difficile. Pour démontrer sa complexité il suffit de démontrer que le problème de couverture par sommets (\emph{vertex cover}) est polynomialement réductible à ce problème.
\begin{thm}
Soit $G = (V,A)$ un graphe orienté et $k$ un entier non négatif. Le problème de décider s'il existe un ensemble $U$ de $k$ sommets tel que $G[V-U]$ est acyclique est NP-complet.
\end{thm}
\dem
Premièrement, nous devons montrer que le problème TCCM est dans NP. Un TCCM est un sous-ensemble de sommets $U \subseteq V$ tels que, en supprimant tous les sommets de $U$, le sous-graphe induit $G [V-U]$ est acyclique. Le problème est dans NP, car la donnée de $U$ est un certificat facilement vérifiable en temps polynomial (la construction du graphe $G [V-U]$ et la vérification qu'il est acyclique est vérifiable en $O(|V|+|A|)$).   

Ensuite, nous démontrons que le problème TCCM est NP-Complet par la réduction du problème de couverture par sommets à ce dernier. Soit $G=(V,A)$ un graphe non orienté et $k$ un entier positif. L'algorithme de vérification est défini comme suit : pour une instance de couverture par sommets de taille $k$ dans $G$ on construit un TCCM dans $G'=(V,A')$ tel que $G'=(V,A')$ est le graphe orienté ayant les mêmes sommets que $G$ et pour chaque arrête $(u,v)$ de $G$ on crée deux arcs $(u,v)$ et $(v,u)$ dans $G'$ (voir figure \ref{vertex-cover}).
\begin{figure}[!h]
\centering
\subfigure[: $G=(V,A)$ \label{vertex-cover1}]{\includegraphics [scale=0.9]{Logos/vertex-cover1}}
\hspace*{1.8cm}
\subfigure[: $G'=(V,A')$ \label{vertex-cover2}]{\includegraphics [scale=0.9]{Logos/vertex-cover2}}
\hspace*{1.8cm}
\subfigure[: Graphe acyclique \label{vertex-cover3}]{\includegraphics [scale=0.9]{Logos/vertex-cover3}}
\caption[Réduction du problème de couverture par sommets au problème TCCM.]{Réduction du problème de couverture par sommets au problème TCCM. (a) Graphe non orienté $G=(V,A)$, l'ensemble $S=\{b,c\}$ est une couverture par sommets du graphe $G$. (b) Le graphe orienté $G'=(V,A')$ associé au graphe $G$, l'ensemble $S=\{b,c\}$ est un TCCM du graphe $G'$. (c) Le graphe acyclique induit par la suppression de $S$.}
\label{vertex-cover}
\end{figure}

Maintenant, nous montrons qu'il existe une couverture par sommets de taille $k$ dans $G$ si et seulement s'il y a un sous-ensemble de $k$ sommets dans $G'$ dont sa suppression casse tous les circuits. Tout d'abord, supposons qu'il existe une couverture par sommets de taille $k$ dans $G$ (dans la figure \ref{vertex-cover1} l'ensemble $S=\{b,c\}$ couvre toutes les arrêtes de $G$). Supprimons ces $k$ sommets de $G'$ avec les arcs incidents de ces sommets. Sachant que pour chaque arc orienté $(u, v) \in G'$, au moins l'un des sommets $u$ et $v$ doit être enlevé, parce que l'un des deux doit être dans la couverture par sommets. Ainsi, après la suppression de ces $k$ sommets et leurs arcs incidents de $G'$, on ne trouve aucune paire de sommets ayant un arc entre eux, et par conséquent il n'existe aucun circuit.

Inversement, supposons qu'il existe un ensemble $S$ de $k$ sommets dans $G'$ dont sa suppression casse tous les circuits. Par construction de chaque paire de sommets $u$, $v$ de $S$, chaque circuit dans $G'$ doit contenir au moins un sommet de $S$. En particulier, chaque circuit de longueur 2 de $G'$ doit contenir au moins un sommet de $S$. Chaque circuit de longueur 2 correspond à une arrête dans $G$. Donc, chaque arrête dans $G$ a au moins l'une de ses extrémités dans $S$, et donc $S$ est une couverture par sommet de $G$.

Après avoir démontré que le problème TCCM est NP-difficile, on présente maintenant une approche permettant de calculer un TCCM en se basant sur la réduction du graphe.

\section{Opérateurs de contraction de Levy et Low}

Premièrement, on commence par l'introduction de la notation de trois opérations utilisées pour la description des opérateurs de contraction définis par Levy et Low. Soit $G=(V,A)$ un graphe orienté et $u$, $v$ et $w$ des sommets appartient à $V$. L'opération \del$(u)$ supprime le sommet $u$ et tous ses arcs incidents $A(u)$, l'opération \mer$(u,v)$ fusionne les deux sommets $u$ et $v$ et l'opération \van$(u)$ supprime le sommet $u$ tout en préservant les liens entre ses prédécesseurs $N^-(u)$ et ses successeurs $N^+(u)$.
\begin{enumerate}
	\item $G.\del(u)$ est le graphe $G' = (V',A')$, où $V'=V - \{u\}$ et $A' = A - A(u)$.
	\item $G.$\mer$(u,v)$ est le graphe $G' = (V',A')$, où $V'=V - \{v\}$ et $A' = (A \cup \{(w,u)|w \in N^-(v)\} \cup \{(u,w)|w\in N^+(v)\}) - A(v).$
	\item $G.$\van$(u)$ est le graphe $G' = (V',A')$, où $V'=V - \{u\}$ et $A' = (A \cup \{(v,w)|v \in N^-(u) , w \in N^+(u)\}) - A(u).$
\end{enumerate}

Levy et Low ont défini cinq opérateurs de contraction qui simplifient le calcul d'un TCCM dans un graphe orienté, ces opérateurs sont les suivantes : 
\begin{defn} Soit $G=(V,A)$ un graphe orienté et $u \in V$. Alors
\begin{enumerate}
\item $G.\iz(u) = G.\del(u)$, si $u$ est une source;
\item $G.\oz(u) = G.\del(u)$, si $u$ est un puits;
\item $G.\lo(u) = G.\del(u)$, si $(u,u) \in A $;
\item $G.\iu(u) = G.\mer(v,u)$, si $N^-(u)= \{v\}$;
\item $G.\ou(u) = G.\mer(v,u)$, si $N^+(u)= \{v\}$. 
\end{enumerate}
\end{defn}

Soit $G'=(V',A')$ le graphe orienté résultant de $G$ par l'application répétée des opérateurs de contraction jusqu'à ce qu'aucune contraction ne soit possible, $G'$ est appelé  \emph{le graphe contracté} de $G$. Pour tout graphe orienté $G$ ne contenant pas d'arcs parallèles, il existe un unique graphe contracté $G'$. L'importance de cette propriété est qu'elle permet d'appliquer les opérateurs de contraction dans n'importe quel ordre sans affecter le résultat final.
\begin{thm}
\label{thm9}
Soit $G=(V,A)$ un graphe orienté, $u \in V$ un sommet, 
\begin{enumerate}
	\item Si $G$ est contracté en $G’$ par l’application d’un opérateur \iz$(u)$, \oz$(u)$, \iu$(u)$ ou \ou$(u)$, et $S’$ est un TCCM de $G’$ alors $S’$ est un TCCM de $G$;
	\item Si $G$ est contracté en $G’$ par l’application de l’opérateur \lo$(u)$, et $S’$ est un TCCM de $G’$ alors $S=S' \cup \{u\}$ est un TCCM de $G$.
\end{enumerate}
	\label{thmm}
 \end{thm}
\dem
\begin{enumerate}
	\item Soit $U \in TCCM(G)$, nous montrons que $U$ est un TC de $G'$. Soit $c$ un circuit de $G'$. Si $u$ est une source ou un puits, alors $u \notin c$, et parce que $c$ est aussi un circuit de $G$, il est donc couvert par $U$. Si $c$ n'est pas un circuit de $G$, il est forcément la réduction d'un circuit de $G$ issu de la suppression d'un sommet $u$ par l'application de \iu($u$) ou \ou($u$), donc, il est couvert par $U$. Il reste à prouver la minimalité de $U$ pour $G$. Supposons qu'il existe $U' \in TCCM (G')$ tel que $|U'|<|U|$, donc, $U'$ est un TC de $G$, c'est une contradiction de la minimalité de $U$ pour $G$.
  \item Soit $U \in TCCM(G)$. Premièrement, nous montrons que $U - \{u\}$ est un TC de $G'$. Soit $c$ un circuit de $G'$, alors $c$ est également un circuit de $G$ et il ne passe pas par $u$, donc, il est couvert par $U$. La minimalité de $U-\{u\}$ résulte de la minimalité de $U$ et le fait que l'arc $(u,u)$ doit aussi être couvert.
\end{enumerate}

\begin{example}
La figure \ref{contra} montre un exemple de réduction d'un graphe avec les cinq opérateurs de Levy et Low. Tout d'abord, les opérateurs \iz\ et \oz\ sont appliqués sur les sommets 1 et 2. Ensuite, les opérateurs \ou\ et \iu\ sont appliqués sur 0 et 5 et, Enfin, l'opérateur \lo\ est appliqué sur 3 et 4, ce qui donne le graphe vide. Donc, l'ensemble $S=\{3, 4\}$ est un TCCM du graphe initial.
\vspace*{-0.1cm}
\begin{figure}[!h]
\centering
 \includegraphics [scale=0.8]{Logos/op_levy}
\caption{Réduction d'un graphe en appliquant les cinq opérateurs de Levy et Low.}
\label{contra}
\end{figure}
\end{example}
\begin{thm} Soit $G=(V,A)$ un graphe orienté de $n$ sommets et $m$ arcs. Si $G$ est complètement réductible au graphe vide par l'application des opérateurs de Levy et Low. Alors, on peut calculer un TCCM de $G$ en temps polynomial égal à $\mathcal{O}(m \log(n))$.
\end{thm}
\dem La complexité de l'algorithme est liée au nombre de suppressions et de déplacement d'arcs, chaque suppression ou déplacement exige $\mathcal{O}(1)$, chaque arc peut être déplacé $\log(n)$ fois au maximum par les opérateurs \iu\ et \ou, donc la complexité totale est $\mathcal{O}(m \log(n))$.
 
Si chaque nœud du graphe a plus d'un arc entrant et plus d'un arc sortant, l’ensemble des opérateurs de Levy et Low ne s’appliquent pas sur le graphe et ne permettent pas de calculer un TCCM.  Néanmoins, cette ligne de travail a un impact significatif dans l'étude du problème TCCM pour deux raisons. Premièrement, on peut trouver un TCCM pour tout graphe réductible même s'il est de grande taille. Deuxièmement, la plupart des algorithmes exacts proposés ultérieurement utilisent ces opérations de réduction. 

\section{Opérateurs de contraction de Lin et Jou}
En 2000, Lin et Jou \cite{Lin2000} ont ajouté trois autres opérateurs de réduction et ont présenté un algorithme exact basé sur la technique séparation et évaluation permettant de calculer un TCCM en temps polynomial pour des graphes orientés.

\subsection{L'opérateur \pie}

Soient $G=(V,A)$ un graphe orienté, $u, v \in V$ des sommets et $e=(u,v) \in A$ un arc. L'arc $e$ est acyclique si aucun circuit de $G$ passe par $e$, dans la figure \ref{pie_a}, les arcs en gras sont des arcs acycliques. L'arc $e=(u,v)$ est appelé $\pi-arc$ si $(v,u)$ est un arc de $G$, dans la figure \ref{pie_c}, tous les arcs sont des $\pi-arcs$. L'arc $e$ est appelé $\pi-acyclique$ s'il est un arc acyclique dans le sous-graphe $G-\pi(G)$ tel que $\pi (G)$ est un sous-graphe de $G$ composé de tous les $\pi-arcs$ de $G$. Dans la figure \ref{pie_b}, tous les arcs en gras sont de type $\pi-acyclique$ (voir figure \ref{pie_d}).
\begin{figure}[!h]
\centering
\subfigure[: $G'=(V',A')$\label{pie_a}] {\fbox{\includegraphics [scale=0.7]{Logos/pie0}}}
\subfigure[: $G=(V,A)$ \label{pie_b}]{\fbox{\includegraphics [scale=0.7]{Logos/pie1}}}
\subfigure[: $\pi (G)$ \label{pie_c}]{\fbox{\includegraphics [scale=0.7]{Logos/pie2}}}
\subfigure[: $G- \pi(G)$\label{pie_d}]{\fbox{\includegraphics [scale=0.7]{Logos/pie3}}}
\caption{Les sous-graphes $\pi (G)$ et $G- \pi(G)$ d'un graphe $G=(V,A)$.}
\label{pie}
\end{figure}
\begin{lemme}\label{L:PIE} Étant donné un graphe $G$, un arc $e=(u,v)$ et un circuit $c$ de $G$.
\begin{enumerate}
	\item \label{lemme1-1} L'arc $e=(u,v)$ est acyclique dans $G$ si et seulement si $u$ et $v$ appartient à deux différentes composante fortement connexes de $G$;
	\item \label{lemme1-2} Tout circuit $c$ passant par un arc $\pi-acyclique$ de $G$, passe aussi par un $\pi-arc$ de $G$.
\end{enumerate}
\end{lemme}
\dem 
\begin{enumerate}
	\item Il existe un circuit qui passe par $(u,v)$ si et seulement si $u\rightarrow v$ et $v\rightarrow u$, ce qui est vrai si et seulement si $u$ et $v$ appartiennent à la même composante fortement connexe.
  \item Supposant qu'il existe un circuit $c$ qui traverse un arc $\pi-acyclique$ $e$, mais il ne traverse aucun $\pi-arc$ de $G$. Soit $G'=G-\pi(G)$, tel que $\pi(G)$ est le sous-graphe de $G$ induit par les $\pi-arcs$ de $G$. Donc $c$ est un circuit de $G'$. Mais $c$ traverse $e$, qui est acyclique dans $G'$ contredisant la condition \ref{L:PIE}-\ref{lemme1-1} du lemme \ref{L:PIE}.
\end{enumerate}

D'après le lemme \ref{L:PIE}, les arcs non utilisés par les circuits du graphe $G$ et les arcs non utilisés par les circuits du graphe dans lequel on a retiré les circuits de type $(u,v,u)$ ne sont pas nécessaires dans le calcul d'un TCCM. Donc la suppression de ces arcs n'affecte pas le calcul d'un TCCM. 
 
\begin{defn} L'opérateur \pie\ supprime les arcs acycliques et $\pi-acyclique$ de $G$. 
\end{defn} 
\begin{example} La figure \ref{gpie}, montre un exemple de réduction d'un graphe $G=(V,A)$ par l'application de l'opérateur \pie\ et les opérateurs de Levy et Low. Au début, on ne peut appliquer aucun opérateur de Levy et Low car, pour chaque sommet $u$ de $G$, $deg^-(u) > 1$ et $deg^+(u)> 1$ et il n'existe pas d'arc de type $e=(u,u)$. L'application de l'opérateur \pie\ sur le graphe initial permet de supprimer les arcs acycliques (arcs lignés) et les arcs $\pi$-acycliques (arcs pointillés). L'application de l'opérateur \lo\ sur les sommets 0, 1, 5 et 7 donne le graphe vide, cela signifie que l'ensemble $S=\{0, 1,5,7\}$ est un TCCM du graphe initial.
\end{example}
\vspace*{-0.4cm}
\begin{figure}[!h]
\centering
 \includegraphics [scale=0.6]{Logos/gpie2}
\caption[Réduction d'un graphe en utilisant l'opérateur \pie.]{Réduction d'un graphe en utilisant l'opérateur \pie\ et les opérateurs de Levy et Low.}
\label{gpie}
\end{figure}

\subsection{L'opérateur \core}

Soient $G=(V,A)$ un graphe orienté et $u \in V$ un sommet. On définit le voisinage du sommet $u$ par \nei$(u) = N(u) \cup \{u\}$. Soit $G'=(V',A')$ un sous-graphe de $G$. Si chaque sommet $u'$ de $V'$ n'a pas de boucle et quels que soient $u', v' \in V'$ tel que $u'\neq v'$, si $(u', v') \in A'$, alors, $G'$ est une \emph {clique}. Un sommet $u$ est un \emph{noyau} de $G’=(V',A')$ si $u \in V'$ et $\forall v \in V', (v,u) \in A'$. Dans la figure \ref{core}, le sous-graphe $ G'=(V',A')$ tel que $V'=\{0,1,2\}$ et $A' = V'\times V'$ est une clique. Le sommet $0$ est un noyau de $G'$. 

\begin{lemme}\label{L:clique} Étant donné un graphe $G=(V,A)$. Soit $G'=(V',A')$ une clique de $G$ ayant $n=|V'|$ sommets. Alors, il existe au minimum $n-1$ sommets de $G'$ dans chaque TCCM de $G$.
\end{lemme}
\dem  Soit $U$ un TCCM de $G$. S'il existe moins de $n-1$ sommet de $G'$ dans $U$ alors, le graphe $G-U$ contient au moins deux sommets $u$ et $v$ qui appartient à $V'$. Parce que $G'$ est une clique, il existe alors un $\pi-arc$ $(u,v)$ dans $G-U$. Donc, il existe un circuit $(u,v,u)$ dans $G-U$. C'est une contradiction. 

Si \nei$(u)$ forme une clique dans un graphe et $u$ un noyau de cette clique, on peut retirer \nei$(u)$ du graphe et on ajoute l'ensemble des sommets voisins de $u$ au TCCM courant. 
\begin{defn}   
 L’opérateur \core\ retire les voisins d'un noyau d'une clique et les ajoute à la solution courante. 
\end{defn}
\begin{example}
La Figure \ref{core}, montre un exemple de réduction d'un graphe $G=(V,A)$ par l'application de l'opérateur \core\ et les opérateurs de Levy et Low. Au début, on ne peut appliquer aucun opérateur de Levy et Low, et l'opérateur \pie\ donne la composante fortement connexe composée du sommets $\{0,1,2\}$. La composante \nei(0) = $\{0,1,2\}$ forme une clique, le sommet $\{0\}$ est son noyau. L'opérateur \core\ supprime \nei(0) et ajoute $\{1, 2\}$ à la solution courante. L'opérateur \lo(3) ajoute le sommet $3$ à la solution, donc l'ensemble $S=\{1,2,3\}$ est un TCCM du graphe initial.
\end{example}
\begin{figure}[!h]
\centering
 \includegraphics [scale=0.75]{Logos/core}
\caption[Réduction d'un graphe en utilisant l'opérateur \core.]{Réduction d'un graphe en utilisant l'opérateur \core\ et les opérateurs de Levy et Low.}
\label{core}
\end{figure}

\subsection{L'opérateur \dome}

Soit $G=(V,A)$ un graphe orienté et $e=(u,v)$ un arc. Si $e=(u,v)$ est un $\pi -arc$, on appelle $u$ un $\pi -$\emph{prédécesseur} de $v$ et $v$ un $\pi-successeur$ de $u$. Dans le cas contraire, on dit que $u$ est un $non-\pi-$\emph{prédécesseur} de $v$ et $v$ est un $non-\pi-successeur$ de $u$. Soient $C_1=(v_1,v_2,...,v_n,v_1)$ et $C_2=(u_1,u_2,...,u_m,u_1)$ deux circuits. On dit que le circuit $C_2$ couvre le circuit $C_1$ si chaque sommet $v_i,i=0,1,...,n$ de $C_1$ est traversé par $C_2$. Un circuit est \emph{minimal} s'il ne couvre aucun autre circuit. Un arc $e=(u,v)$ de $G$ est dit \emph{dominé} si tout $non-\pi-$\emph{prédécesseur} de $u$ est un \emph{prédécesseur} de $v$ (l'arc en gras dans la figure \ref{dome}), ou tout $non-\pi-successeur$ de $v$ est un \emph{successeur} de $u$ (les arcs pointillés dans la figure \ref{dome}).
\begin{lemme}\label{L:DOME} Étant donné un graphe $G$, $e=(u,v)$ un arc dominé de $G$ et $c$ un circuit de $G$.
\begin{enumerate}
	\item Pendant le calcul d'un TCCM de $G$, on a besoin de casser seulement les circuits minimaux;
	\item \label{lemme_3_2} Supposons que $c$ traverse $e$ et $c$ n'est pas minimal. Alors, il existe un circuit plus court $c'$ couvert par $c$ et traversant $e$.
\end{enumerate}
\end{lemme}
\dem
\begin{enumerate}
	\item Pour tout circuit non minimal $c$, il existe un circuit minimal $c_m$ couvert par $c$. Casser le circuit $c_m$ va permettre de casser automatiquement $c$. Donc, on a besoin de casser seulement les circuits minimaux.
	\item $c$ traverse $e=(u,v)$, i.e $c=(c_1,c_2,...,c_i,u,v,c_{i+1},c_{i+2},...,c_k,c_1)$ pour $i$, $k$ tel que $1\leq i<k$. Il existe 3 cas possibles :
	
	\begin{enumerate}
		\item Si $c_i$ est un $\pi -$\emph{prédécesseur} de $u$, le circuit $c'=(c_i,u,c_i)$ est couvert par $c$; 
		\item Si $c_{i+1}$ est un $\pi-successeur$ de $v$, le circuit $c'=(c_{i+1},v,c_{i+1})$ est couvert par $c$;
		\item Si $c_i$ n'est pas un $\pi -$\emph{prédécesseur} de $u$ et $c_{i+1}$ n'est pas un $\pi-successeur$ de $v$. Dans ce cas, d'après la définition d'un arc dominé, soit $c_i$ prédécesseur de $v$ ou $c_{i+1}$ successeur de $u$. En premier cas, le circuit $(c_1,c_2,...,c_i,v,c_{i+1},c_{i+2},...,c_k,c_1)$ est couvert par $c$, dans le deuxième cas, le circuit $(c_1,c_2,...,c_i,u,c_{i+1},c_{i+2},...,c_k,c_1)$ est couvert par $c$.  
	\end{enumerate}
	\end{enumerate}

On conclut du lemme \ref{L:DOME} qu'uniquement les arcs non dominés sont nécessaires dans le calcul d'un TCCM. 
\begin{defn}L'opérateur \dome\ supprime les arcs dominés.
\end{defn}
\begin{example}
La Figure \ref{dome} montre un exemple de réduction d'un graphe $G=(V,A)$ par l'application de l'opérateur \dome\ et les opérateurs de Levy et Low. Les opérateurs de Levy et Low et les opérateurs \pie\ et \core\ ne sont pas applicables sur le graphe initial. On commence à appliquer l'opérateur \dome\ sur le graphe initial, cet opérateur supprime les arcs dominés, puis on continue par l'application des opérateurs de Levy et Low. L'ensemble $S=\{2,4\}$ est un TCCM du graphe initial.
\begin{figure}[!h]
\centering
 \includegraphics [scale=0.7]{Logos/dome2}
\caption[Réduction d'un graphe en utilisant l'opérateur \dome.]{Réduction d'un graphe en utilisant l'opérateur \dome\ et les opérateurs de Levy et Low.}
\label{dome}
\end{figure}
\end{example}

\vspace*{1cm}

\begin{thm}
Soit $G=(V,A)$ un graphe orienté, $u \in V$ un sommet. Alors
\begin{enumerate}
	\item Si $G$ est contracté en $G’$ par l’application de l'opérateur \pie\ ou \dome\, et $S’$ est un TCCM de $G’$ alors $S’$ est un TCCM de $G$;
	\item Si $G$ est contracté en $G’$ par l’application de l'opérateur \core. Si le sommet $u$ est un noyau de la clique retirée par \core\ du graphe $G$ et $S’$ est un TCCM de $G’$, alors $S’ \cup N(u)$ est un TCCM de $G$;
\end{enumerate}
	\label{hgjf}
 \end{thm}
\dem
\begin{enumerate}
	\item Pour l'opérateur \pie. Supposons $U \in TCCM(G)$. D'abord, on doit montrer que $U$ est un TC de $G'$. Soient $B$ l'ensemble des arcs acyclique de $G$, $C$ l'ensemble des arcs $\pi$-acycliques de $G$ et $P=B\cup C$. i.e. $P$ est l'ensemble des arc retirés par \pie. Soit $c$ un circuit de $G'$ et $A(c)$ les arcs de $c$. Si $P\cap A(c) = \emptyset$ alors $c$ est un circuit de $G$ donc il est couvert par $U$. Sinon, supposons qu'il existe un arc $(u,v) \in P \cap A(c)$. Il est clair que $(u,v) \notin B$ tant qu'il n' y a pas de circuit qui traverse un arc acyclique. Par conséquent $(u,v)$ est $\pi-acyclique$ donc $c$ passe par un arc $(u,v)$ d'après le lemme \ref{L:PIE}-\ref{lemme1-2}. Mais $(u,v,u)$ est un circuit de $G$ et de $G'$ et il est couvert par $U$, ce qui implique que $c$ est aussi couvert par $U$. Maintenant, on doit montrer que $U$ est minimal. Par contradiction, supposons qu'il existe $U' \in TCCM(G')$ tel que $|U'|<|U|$. Comme $TCCM(G') \subseteq TCCM(G)$, on a $U' \in TCCM(G)$, contredisant la minimalité de $U$ dans $G$. Pour l'opérateur \dome, supposons que $U \in TCCM(G)$. On prouve que $U$ est un TC de $G'$. Soit $c$ un circuit de $G'$. Si le circuit $c$ ne traverse aucun arc dominé, alors il est aussi un circuit de $G$, et donc il est couvert par $U$. Sinon, supposons que $c$ traverse un arc dominé $e$. Alors il existe un circuit $c'$ plus court et couvert par $c$ traverse $e$ d'après le lemme \ref{L:DOME}-\ref{lemme_3_2}. Alors $c'$ est aussi un circuit de $G$, donc il est couvert par $U$. La minimalité est triviale. 
	\item Supposons que $U \in TCCM(G)$. Soit $D$ la clique de $G$ retiré par \core. On montre que $U$ est un TC de $G'\cup D$. Soit $c$ un circuit de $G'\cup D$. Si $c \notin D$, alors $c$ est un circuit de $G'$ et il est aussi un circuit de $G$ donc il est couvert par $U$. Sinon, d'après le lemme \ref{L:clique} pour casser tous les circuits de $D$, juste un seul sommet peut être resté et tous les arcs doivent être enlevés. Tant que tous les arcs incidents d'un noyau $u$ de $D$ sont dans $D$, l'ajout de $N(u)$ à $U$ casse tout circuit $c$ de $D$ donc $c$ est couvert par $U$. La minimalité de $U$ est garantie par le lemme \ref{L:clique}.
	
\end{enumerate}
Lin et Jou ont défini une classe de graphes orientés notés \dome-\emph{contractible}. Plus précisément, on dit qu'un graphe orienté est \dome-\emph{contractible} si l'application successive des huit opérateurs résulte en un graphe vide. L'ensemble des sommets ajoutés par les opérateurs \lo\ et \core\ forment un TCCM du graphe initial.

\begin{thm}
Soit $G=(V,A)$ un graphe orienté \dome-contractible avec $n$ sommets et $m$ arcs.  Alors on peut calculer un TCCM de G en temps polynomial égal à $O(m^2n)$.
\end{thm}
\dem
Les opérateurs \iz, \oz, \iu, \ou\ et \lo\ ont un temps égal à $O(m+n)$. L'opérateur \pie\ est aussi linéaire ($O(m+n)$). L'opérateur \core\ s’exécute en temps égal à $O(m+n \log n)$ et \dome\ prend un temps égal à $O(m.n)$. A chaque étape de réduction au moins un sommet ou un arc est supprimé, la complexité globale est bornée par $O((m.(mn)) = O(m^2n)$.

Lin et Jou ont présenté un algorithme exact basé sur la technique séparation et évaluation permettant de calculer un TCCM en temps polynomial pour n'importe quel graphe orienté (voir l'algorithme \ref{un_tccm}).  
\begin{algorithm}[!ht]
\caption{L'algorithme de Lin et Jou pour calculer un TCCM}
\label{un_tccm}
 \begin{algorithmic}[1]
  \Function{\onemfvs}{$G:$ graphe orienté, S,B :sommets, p,L:entiers}: sommets
 \State Appliquer les opérateurs de réduction sur $G$
 \State Soit $S'$ l’ensemble de sommets enlevés par \lo\ et \core
 \State $S \gets S \cup S'$
 \State $\mathscr{C} \gets \cc(G)$
		\If {$|\mathscr{C}|>1$}
		  \ForAll {$C=(A',V') \in \mathscr{C}$}
         \State $S \gets S$ $\cup$ $\onemfvs(C,0,V',0,0)$
      \EndFor
		  \State \Return $S$
		\Else
		  \State $lowerbound \gets \lb(G)$
			   \If {$L=0$}
				     \State $p \gets lowerbound$
						 \State $ B \gets S$ $\cup$ $\as(G)$
				 \EndIf
				\If {$|S|+lowerbound > |B|$}
				     \State \Return $B$
				\Else
				    \If {$G= \emptyset$}
				      \State \Return $S$
						\EndIf
				\EndIf
			\State $v \gets \mdv(G)$
			\State $S_1 \gets \onemfvs(G.\del(v),S \cup \{v\},B,p,L+1)$
			\If {$|S_1|<|B|$}
				 \State $B \gets S_1$
			\EndIf
			\If {$p=|B|$}
				 \State \Return$B$
			\EndIf
			\State $S_2 \gets \onemfvs(G.\van(v),S,B,p,L+1)$
			\If {$|S_2|<|B|$}
				 \State $B \gets S_2$
			\EndIf
			\State \Return $B$
		\EndIf
 \EndFunction
 \end{algorithmic}
 \end{algorithm}

L'algorithme proposé par Lin et Jou peut être résumé comme suit :
\begin{enumerate}
	\item Réduire le graphe autant que possible en utilisant les huit opérateurs;
	\item S'il y a plus d'une composante connexe, diviser le problème en sous-problèmes selon ces composantes.
	\item Calculer une borne supérieure $U$ pour le graphe en cours;
	\item Si $U$ est la meilleure solution trouvée jusqu'à présent, la sauvegarder;	
	\item Calculer une borne inférieure $L$ du problème pour le graphe en cours en suivant ces étapes :
	 \begin{description}
		 \item[\normalfont(a)] Si le graphe est vide, arrêter;
		 \item[\normalfont(b)] Sinon, soit $c$ un circuit de longueur minimal du graphe;
		 \item[\normalfont(c)] Supprimer $c$ du graphe et incrémenter $L$ de $1$;
		 \item[\normalfont(d)] Appliquer les huit opérateurs de contractions autant que possible;
		 \item[\normalfont(e)] Revenir à l'étape 5(a).
	 \end{description}
	
	\item Si la borne inférieure indique que la solution actuelle ne pourra pas être étendue à une meilleure solution, ne pas poursuivre avec cette solution;
	\item Sinon, choisir un sommet $u$ et effectuer un branchement en deux cas~: soit on inclut le sommet $u$ dans la solution, soit on l'exclut.
\end{enumerate}

\section{Conclusion}

Le problème TCCM est largement étudié depuis son introduction et plusieurs approches ont été proposées pour le résoudre. Dans ce chapitre, on a présenté ce problème dans son cadre général et on a détaillé l'approche définie par Levy et Low, et étendue par Lin et Jou, qui se base sur le principe de réduction du graphe par la suppression des sommets et des arcs inutiles. Cette approche a permis de concevoir des algorithmes polynomiaux pour calculer un TCCM dans un graphe orienté et permet également de concevoir un algorithme énumérant l'ensemble de tous les TCCM comme nous le décrivons dans le chapitre suivant.
